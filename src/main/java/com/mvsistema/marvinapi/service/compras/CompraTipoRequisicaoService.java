package com.mvsistema.marvinapi.service.compras;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.compras.CompraTipoRequisicao;
import com.mvsistema.marvinapi.repository.compras.CompraTipoRequisicaoRepository;

@Service
public class CompraTipoRequisicaoService {

	@Autowired
	private CompraTipoRequisicaoRepository repository;
	
	public List<CompraTipoRequisicao> listaTodos() {
		return repository.findAll();
	}
	
	public List<CompraTipoRequisicao> listaTodos(String nome) {
		return repository.findFirst10ByNomeContaining(nome);
	}
	
	public CompraTipoRequisicao buscaPorId(Long id) {
		try {
			CompraTipoRequisicao compraTipoRequisicao = this.repository.findOne(id);
			if (compraTipoRequisicao == null) {
				throw new EmptyResultDataAccessException(1);
			}	
			return compraTipoRequisicao;
		} catch (NoSuchElementException | IllegalArgumentException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	public CompraTipoRequisicao salvaObjeto(CompraTipoRequisicao compraTipoRequisicao) {
		return repository.save(compraTipoRequisicao);
	}
	
	public void excluirObjeto(Long id) {
		CompraTipoRequisicao compraTipoRequisicao = new CompraTipoRequisicao();
		compraTipoRequisicao.setId(id);
		repository.delete(compraTipoRequisicao);
	}
	
}
