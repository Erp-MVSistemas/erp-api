package com.mvsistema.marvinapi.service.compras;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.compras.CompraCotacao;
import com.mvsistema.marvinapi.model.compras.CompraCotacaoDetalhe;
import com.mvsistema.marvinapi.model.compras.CompraFornecedorCotacao;
import com.mvsistema.marvinapi.model.compras.CompraReqCotacaoDetalhe;
import com.mvsistema.marvinapi.repository.compras.CompraCotacaoRepository;

@Service
public class CompraCotacaoService {

	@Autowired
	private CompraCotacaoRepository repository;
	
	public List<CompraCotacao> listaTodos() {
		return repository.findAll();
	}
	
	public CompraCotacao buscaPorId(Long id) {
		try {
			CompraCotacao compraCotacao = this.repository.findOne(id);
			if (compraCotacao == null) {
				throw new EmptyResultDataAccessException(1);
			}	
			return compraCotacao;
		} catch (NoSuchElementException | IllegalArgumentException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	public CompraCotacao salvaObjeto(CompraCotacao compraCotacao) {
		
		for (CompraFornecedorCotacao cfc : compraCotacao.getListaCompraFornecedorCotacao()) {
			cfc.setCompraCotacao(compraCotacao);
			
			for (CompraReqCotacaoDetalhe crcd : compraCotacao.getListaCompraReqCotacaoDetalhe()) {
				crcd.setCompraCotacao(compraCotacao);
				
				CompraCotacaoDetalhe compraCotacaoDetalhe = new CompraCotacaoDetalhe();
				compraCotacaoDetalhe.setCompraFornecedorCotacao(cfc);
				compraCotacaoDetalhe.setProduto(crcd.getCompraRequisicaoDetalhe().getProduto());
				compraCotacaoDetalhe.setQuantidade(crcd.getQuantidadeCotada());
				
				cfc.getListaCompraCotacaoDetalhe().add(compraCotacaoDetalhe);
			} 
		}
		
		
		
		return repository.save(compraCotacao);
	}
	
	public void excluirObjeto(Long id) {
		CompraCotacao compraCotacao = new CompraCotacao();
		compraCotacao.setId(id);
		repository.delete(compraCotacao);
	}
	
}
