package com.mvsistema.marvinapi.service.compras;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.compras.CompraRequisicao;
import com.mvsistema.marvinapi.model.compras.CompraRequisicaoDetalhe;
import com.mvsistema.marvinapi.repository.compras.CompraRequisicaoRepository;

@Service
public class CompraRequisicaoService {

	@Autowired
	private CompraRequisicaoRepository repository;
	
	public List<CompraRequisicao> listaTodos() {
		return this.repository.findAll();
	}
	
	public List<CompraRequisicaoDetalhe> listaCompraRequisicaoDetalhe(){
		return this.repository.findAllListaCompraRequisicaoDetalhe();
	}
	
	public CompraRequisicao buscaPorId(Long id) {
		try {
			CompraRequisicao compraRequisicao = this.repository.findOne(id);
			if (compraRequisicao == null) {
				throw new EmptyResultDataAccessException(1);
			}	
			return compraRequisicao;
		} catch (NoSuchElementException | IllegalArgumentException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	public CompraRequisicao salvaObjeto(CompraRequisicao compraRequisicao) {
		
		for (CompraRequisicaoDetalhe d : compraRequisicao.getListaCompraRequisicaoDetalhe()) {
			d.setCompraRequisicao(compraRequisicao);
		}
		
		return repository.save(compraRequisicao);
	}
	
	public void excluirObjeto(Long id) {
		CompraRequisicao compraRequisicao = new CompraRequisicao();
		compraRequisicao.setId(id);
		repository.delete(compraRequisicao);
	}
	
}
