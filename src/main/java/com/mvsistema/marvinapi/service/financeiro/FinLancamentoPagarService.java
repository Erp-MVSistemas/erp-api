package com.mvsistema.marvinapi.service.financeiro;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.model.financeiro.FinLancamentoPagar;
import com.mvsistema.marvinapi.model.financeiro.FinParcelaPagar;
import com.mvsistema.marvinapi.model.financeiro.FinStatusParcela;
import com.mvsistema.marvinapi.repository.financeiro.FinLancamentoPagarRepository;

@Service
public class FinLancamentoPagarService {

	@Autowired
	private FinLancamentoPagarRepository repository;
	
	public List<FinLancamentoPagar> listaTodos() {
		return repository.findAll();
	}
	
	public FinLancamentoPagar buscaPorId(Long id) {
		return repository.findOne(id);
	}
	
	public FinLancamentoPagar salvaObjeto(FinLancamentoPagar finLancamentoPagar) {
		FinStatusParcela statusParcela = new FinStatusParcela();
		statusParcela.setId(1L);
		
		BigDecimal valorParcela = finLancamentoPagar.getValorTotal().divide(BigDecimal.valueOf(finLancamentoPagar.getQuantidadeParcela()), 2, RoundingMode.HALF_EVEN);
		BigDecimal valorParcial = valorParcela.multiply(BigDecimal.valueOf(finLancamentoPagar.getQuantidadeParcela()).subtract(new BigDecimal(1)));  
        BigDecimal ultimaParcela = finLancamentoPagar.getValorTotal().subtract(valorParcial);  
		
		Calendar dataAtual = Calendar.getInstance();
		Calendar primeiroVencimento = Calendar.getInstance();
		Date dataFinLancamentoPagar = Date.from(finLancamentoPagar.getPrimeiroVencimento().atStartOfDay(ZoneId.systemDefault()).toInstant());
		primeiroVencimento.setTime(dataFinLancamentoPagar);
		
		if (finLancamentoPagar.getListaFinParcelaPagar() == null) {
			finLancamentoPagar.setListaFinParcelaPagar(new ArrayList<>());
		}
		
		for (int i = 1; i <= finLancamentoPagar.getQuantidadeParcela(); i++) {
			FinParcelaPagar parcelaPagar = new FinParcelaPagar();
			parcelaPagar.setContaCaixa(finLancamentoPagar.getContaCaixa());
			parcelaPagar.setFinLancamentoPagar(finLancamentoPagar);
			parcelaPagar.setFinStatusParcela(statusParcela);
			parcelaPagar.setDataEmissao(dataAtual.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
			parcelaPagar.setNumeroParcela(i);
			if (i == finLancamentoPagar.getQuantidadeParcela()) {  
				parcelaPagar.setValor(ultimaParcela);
            } else {  
            	parcelaPagar.setValor(valorParcela);
            }
			
			if (i == 1) {
				parcelaPagar.setDataVencimento(primeiroVencimento.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
			} else {
				primeiroVencimento.add(Calendar.DAY_OF_MONTH, finLancamentoPagar.getIntervaloEntreParcelas());
				parcelaPagar.setDataVencimento(primeiroVencimento.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
			}

			finLancamentoPagar.getListaFinParcelaPagar().add(parcelaPagar);
		}
		
		return repository.save(finLancamentoPagar);
	}
	
	public void excluirObjeto(Long id) {
		FinLancamentoPagar finLancamentoPagar = new FinLancamentoPagar();
		finLancamentoPagar.setId(id);
		repository.delete(finLancamentoPagar);
	}
	
}
