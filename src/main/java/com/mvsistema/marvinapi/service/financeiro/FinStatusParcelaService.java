package com.mvsistema.marvinapi.service.financeiro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.model.financeiro.FinStatusParcela;
import com.mvsistema.marvinapi.repository.financeiro.FinStatusParcelaRepository;

@Service
public class FinStatusParcelaService {

	@Autowired
	private FinStatusParcelaRepository repository;
	
	public List<FinStatusParcela> listaTodos() {
		return repository.findAll();
	}
	
	public List<FinStatusParcela> listaTodos(String nome) {
		return repository.findFirst10ByDescricaoContaining(nome);
	}
	
	public FinStatusParcela buscaPorId(Long id) {
		return repository.findOne(id);
	}
	
	public FinStatusParcela salvaObjeto(FinStatusParcela finStatusParcela) {
		return repository.save(finStatusParcela);
	}
	
	public void excluirObjeto(Long id) {
		FinStatusParcela finStatusParcela = new FinStatusParcela();
		finStatusParcela.setId(id);
		repository.delete(finStatusParcela);
	}
	
}
