package com.mvsistema.marvinapi.service.financeiro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.model.financeiro.FinNaturezaFinanceira;
import com.mvsistema.marvinapi.repository.financeiro.FinNaturezaFinanceiraRepository;

@Service
public class FinNaturezaFinanceiraService {

	@Autowired
	private FinNaturezaFinanceiraRepository repository;
	
	public List<FinNaturezaFinanceira> listaTodos() {
		return repository.findAll();
	}
	
	public List<FinNaturezaFinanceira> listaTodos(String nome) {
		return repository.findFirst10ByDescricaoContaining(nome);
	}
	
	public FinNaturezaFinanceira buscaPorId(Long id) {
		return repository.findOne(id);
	}
	
	public FinNaturezaFinanceira salvaObjeto(FinNaturezaFinanceira finNaturezaFinanceira) {
		return repository.save(finNaturezaFinanceira);
	}
	
	public void excluirObjeto(Long id) {
		FinNaturezaFinanceira finNaturezaFinanceira = new FinNaturezaFinanceira();
		finNaturezaFinanceira.setId(id);
		repository.delete(finNaturezaFinanceira);
	}
	
}
