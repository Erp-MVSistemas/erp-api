package com.mvsistema.marvinapi.service.financeiro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.model.financeiro.FinParcelaPagamento;
import com.mvsistema.marvinapi.model.financeiro.FinParcelaPagar;
import com.mvsistema.marvinapi.repository.financeiro.FinParcelaPagamentoRepository;

@Service
public class FinParcelaPagamentoService {

	@Autowired
	private FinParcelaPagamentoRepository repository;
	
	public List<FinParcelaPagar> listaTodos() {
		return repository.findAll();
	}
	
	public FinParcelaPagar buscaPorId(Long id) {
		return repository.findOne(id);
	}
	
	public FinParcelaPagar salvaObjeto(FinParcelaPagar finParcelaPagamento) {
		List<FinParcelaPagamento> pagamentos = finParcelaPagamento.getListaFinParcelaPagamento();
		
		finParcelaPagamento = repository.findOne(finParcelaPagamento.getId());
		finParcelaPagamento.getListaFinParcelaPagamento().addAll(pagamentos);
		
		for (FinParcelaPagamento p : finParcelaPagamento.getListaFinParcelaPagamento()) {
			p.setFinParcelaPagar(finParcelaPagamento);
		}
		return repository.save(finParcelaPagamento);
	}
	
	public void excluirObjeto(Long id) {
		FinParcelaPagar finParcelaPagamento = new FinParcelaPagar();
		finParcelaPagamento.setId(id);
		repository.delete(finParcelaPagamento);
	}
	
}
