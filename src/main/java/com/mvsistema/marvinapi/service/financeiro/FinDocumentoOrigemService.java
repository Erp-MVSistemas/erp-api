package com.mvsistema.marvinapi.service.financeiro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.model.financeiro.FinDocumentoOrigem;
import com.mvsistema.marvinapi.repository.financeiro.FinDocumentoOrigemRepository;

@Service
public class FinDocumentoOrigemService {

	@Autowired
	private FinDocumentoOrigemRepository repository;
	
	public List<FinDocumentoOrigem> listaTodos() {
		return repository.findAll();
	}
	
	public List<FinDocumentoOrigem> listaTodos(String nome) {
		return repository.findFirst10BySiglaDocumentoContaining(nome);
	}
	
	public FinDocumentoOrigem buscaPorId(Long id) {
		return repository.findOne(id);
	}
	
	public FinDocumentoOrigem salvaObjeto(FinDocumentoOrigem finDocumentoOrigem) {
		return repository.save(finDocumentoOrigem);
	}
	
	public void excluirObjeto(Long id) {
		FinDocumentoOrigem finDocumentoOrigem = new FinDocumentoOrigem();
		finDocumentoOrigem.setId(id);
		repository.delete(finDocumentoOrigem);
	}
	
}
