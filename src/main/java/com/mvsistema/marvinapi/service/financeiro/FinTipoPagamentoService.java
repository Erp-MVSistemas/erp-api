package com.mvsistema.marvinapi.service.financeiro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.model.financeiro.FinTipoPagamento;
import com.mvsistema.marvinapi.repository.financeiro.FinTipoPagamentoRepository;

@Service
public class FinTipoPagamentoService {

	@Autowired
	private FinTipoPagamentoRepository repository;
	
	public List<FinTipoPagamento> listaTodos() {
		return repository.findAll();
	}
	
	public List<FinTipoPagamento> listaTodos(String nome) {
		return repository.findFirst10ByDescricaoContaining(nome);
	}
	
	public FinTipoPagamento buscaPorId(Long id) {
		return repository.findOne(id);
	}
	
	public FinTipoPagamento salvaObjeto(FinTipoPagamento finTipoPagamento) {
		return repository.save(finTipoPagamento);
	}
	
	public void excluirObjeto(Long id) {
		FinTipoPagamento finTipoPagamento = new FinTipoPagamento();
		finTipoPagamento.setId(id);
		repository.delete(finTipoPagamento);
	}
	
}
