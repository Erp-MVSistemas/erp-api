package com.mvsistema.marvinapi.service.estoque;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.model.estoque.EstoqueContagemCabecalho;
import com.mvsistema.marvinapi.model.estoque.EstoqueContagemDetalhe;
import com.mvsistema.marvinapi.repository.estoque.EstoqueContagemCabecalhoRepository;

@Service
public class EstoqueContagemCabecalhoService {

	@Autowired
	private EstoqueContagemCabecalhoRepository repository;
	
	public List<EstoqueContagemCabecalho> listaTodos() {
		return repository.findAll();
	}
	
	public EstoqueContagemCabecalho buscaPorId(Long id) {
		return repository.findOne(id);
	}
	
	public EstoqueContagemCabecalho salvaObjeto(EstoqueContagemCabecalho estoqueContagemCabecalho) {
		for (EstoqueContagemDetalhe d : estoqueContagemCabecalho.getListaEstoqueContagemDetalhe()) {
			d.setEstoqueContagemCabecalho(estoqueContagemCabecalho);
			d.setQuantidadeSistema(d.getProduto().getQuantidadeEstoque());
			//TODO: analisar se esse processo esta correto
			d.setAcuracidade(d.getQuantidadeContada().divide(d.getQuantidadeSistema()));
			d.setDivergencia(d.getQuantidadeContada().subtract(d.getQuantidadeSistema()));
		}
		return repository.save(estoqueContagemCabecalho);
	}
	
	public void excluirObjeto(Long id) {
		EstoqueContagemCabecalho estoqueContagemCabecalho = new EstoqueContagemCabecalho();
		estoqueContagemCabecalho.setId(id);
		repository.delete(estoqueContagemCabecalho);
	}
	
}
