package com.mvsistema.marvinapi.service.estoque;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.model.cadastro.Produto;
import com.mvsistema.marvinapi.model.estoque.EstoqueReajusteCabecalho;
import com.mvsistema.marvinapi.model.estoque.EstoqueReajusteDetalhe;
import com.mvsistema.marvinapi.repository.cadastro.ProdutoRepository;
import com.mvsistema.marvinapi.repository.estoque.EstoqueReajusteCabecalhoRepository;

@Service
public class EstoqueReajusteCabecalhoService {

	@Autowired
	private EstoqueReajusteCabecalhoRepository repository;
	@Autowired
	private ProdutoRepository repositoryProduto;
	
	public List<EstoqueReajusteCabecalho> listaTodos() {
		return repository.findAll();
	}
	
	public EstoqueReajusteCabecalho buscaPorId(Long id) {
		return repository.findOne(id);
	}
	
	public EstoqueReajusteCabecalho salvaObjeto(EstoqueReajusteCabecalho estoqueReajusteCabecalho) {
		for (EstoqueReajusteDetalhe d : estoqueReajusteCabecalho.getListaEstoqueReajusteDetalhe()) {
			d.setEstoqueReajusteCabecalho(estoqueReajusteCabecalho);
			d.setValorOriginal(d.getProduto().getValorVenda());
			
			BigDecimal reajuste = d.getProduto().getValorVenda().multiply(estoqueReajusteCabecalho.getPorcentagem());

			Produto produto = d.getProduto();
			if (estoqueReajusteCabecalho.getTipoReajuste().equals("A")) {
				produto.setValorVenda(produto.getValorVenda().add(reajuste));
			} else {
				produto.setValorVenda(produto.getValorVenda().subtract(reajuste));
			}
			repositoryProduto.save(produto);
			d.setValorReajuste(produto.getValorVenda());
		}
		return repository.save(estoqueReajusteCabecalho);
	}
	
	public void excluirObjeto(Long id) {
		EstoqueReajusteCabecalho estoqueReajusteCabecalho = new EstoqueReajusteCabecalho();
		estoqueReajusteCabecalho.setId(id);
		repository.delete(estoqueReajusteCabecalho);
	}
	
}
