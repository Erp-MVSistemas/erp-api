package com.mvsistema.marvinapi.service.vendas;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.vendas.VendaCondicoesPagamento;
import com.mvsistema.marvinapi.repository.vendas.VendaCondicoesPagamentoRepository;

@Service
public class VendaCondicoesPagamentoService {

	@Autowired
	private VendaCondicoesPagamentoRepository repository;
	
	public List<VendaCondicoesPagamento> listaTodos() {
		return repository.findAll();
	}
	
	public List<VendaCondicoesPagamento> listaTodos(String nome) {
		return repository.findFirst10ByNomeContaining(nome);
	}
	
	public VendaCondicoesPagamento buscaPorId(Long id) {
		try {
			VendaCondicoesPagamento vendaCondicoesPagamento = this.repository.findOne(id);
			if (vendaCondicoesPagamento == null) {
				throw new EmptyResultDataAccessException(1);
			}	
			return vendaCondicoesPagamento;
		} catch (NoSuchElementException | IllegalArgumentException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	public VendaCondicoesPagamento salvaObjeto(VendaCondicoesPagamento vendaCondicoesPagamento) {
		return repository.save(vendaCondicoesPagamento);
	}
	
	public void excluirObjeto(Long id) {
		VendaCondicoesPagamento vendaCondicoesPagamento = new VendaCondicoesPagamento();
		vendaCondicoesPagamento.setId(id);
		repository.delete(vendaCondicoesPagamento);
	}
	
}
