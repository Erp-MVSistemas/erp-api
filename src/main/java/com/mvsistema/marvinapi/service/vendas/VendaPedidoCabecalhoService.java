package com.mvsistema.marvinapi.service.vendas;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.vendas.VendaPedidoCabecalho;
import com.mvsistema.marvinapi.model.vendas.VendaDetalhe;
import com.mvsistema.marvinapi.repository.vendas.VendaPedidoCabecalhoRepository;

@Service
public class VendaPedidoCabecalhoService {

	@Autowired
	private VendaPedidoCabecalhoRepository repository;
	
	public List<VendaPedidoCabecalho> listaTodos() {
		return repository.findAll();
	}
	
	public List<VendaPedidoCabecalho> listaTodos(Integer codigo) {
		return repository.findFirst10ByNumeroFaturaContaining(codigo);
	}
	
	public VendaPedidoCabecalho buscaPorId(Long id) {
		try {
			VendaPedidoCabecalho vendaCabecalho = this.repository.findOne(id);
			if (vendaCabecalho == null) {
				throw new EmptyResultDataAccessException(1);
			}	
			return vendaCabecalho;
		} catch (NoSuchElementException | IllegalArgumentException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	public VendaPedidoCabecalho salvaObjeto(VendaPedidoCabecalho vendaCabecalho) {
		for (VendaDetalhe vod : vendaCabecalho.getListaVendaDetalhe()) {
			vod.setVendaCabecalho(vendaCabecalho);
			
		}
		return repository.save(vendaCabecalho);
	}
	
	public void excluirObjeto(Long id) {
		VendaPedidoCabecalho vendaCabecalho = new VendaPedidoCabecalho();
		vendaCabecalho.setId(id);
		repository.delete(vendaCabecalho);
	}
	
}
