package com.mvsistema.marvinapi.service.vendas;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.cadastro.Cargo;
import com.mvsistema.marvinapi.model.vendas.TipoNotaFiscal;
import com.mvsistema.marvinapi.repository.vendas.TipoNotaFiscalRepository;

@Service
public class TipoNotaFiscalService {

	@Autowired
	private TipoNotaFiscalRepository repository;
	
	public List<TipoNotaFiscal> listaTodos() {
		return repository.findAll();
	}
	
	public List<TipoNotaFiscal> listaTodos(String nome) {
		return repository.findFirst10ByModeloContaining(nome);
	}
	
	public TipoNotaFiscal buscaPorId(Long id) {
		try {
			TipoNotaFiscal tipoNotaFiscal = this.repository.findOne(id);
			if (tipoNotaFiscal == null) {
				throw new EmptyResultDataAccessException(1);
			}	
			return tipoNotaFiscal;
		} catch (NoSuchElementException | IllegalArgumentException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}

	}
	
	public TipoNotaFiscal salvaObjeto(TipoNotaFiscal tipoNotaFiscal) {
		return repository.save(tipoNotaFiscal);
	}
	
	public void excluirObjeto(Long id) {
		TipoNotaFiscal tipoNotaFiscal = new TipoNotaFiscal();
		tipoNotaFiscal.setId(id);
		repository.delete(tipoNotaFiscal);
	}
	
}
