package com.mvsistema.marvinapi.service.vendas;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.vendas.VendaOrcamentoCabecalho;
import com.mvsistema.marvinapi.model.vendas.VendaOrcamentoDetalhe;
import com.mvsistema.marvinapi.repository.vendas.VendaOrcamentoCabecalhoRepository;

@Service
public class VendaOrcamentoCabecalhoService {

	@Autowired
	private VendaOrcamentoCabecalhoRepository repository;
	
	public List<VendaOrcamentoCabecalho> listaTodos() {
		return repository.findAll();
	}
	
	public List<VendaOrcamentoCabecalho> listaTodos(String codigo) {
		return repository.findFirst10ByCodigoContaining(codigo);
	}
	
	public VendaOrcamentoCabecalho buscaPorId(Long id) {
		try {
			VendaOrcamentoCabecalho vendaOrcamentoCabecalho = this.repository.findOne(id);
			if (vendaOrcamentoCabecalho == null) {
				throw new EmptyResultDataAccessException(1);
			}	
			return vendaOrcamentoCabecalho;
		} catch (NoSuchElementException | IllegalArgumentException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	public VendaOrcamentoCabecalho salvaObjeto(VendaOrcamentoCabecalho vendaOrcamentoCabecalho) {
		for (VendaOrcamentoDetalhe vod : vendaOrcamentoCabecalho.getListaVendaOrcamentoDetalhe()) {
			vod.setVendaOrcamentoCabecalho(vendaOrcamentoCabecalho);
			
		}
		return repository.save(vendaOrcamentoCabecalho);
	}
	
	public void excluirObjeto(Long id) {
		VendaOrcamentoCabecalho vendaOrcamentoCabecalho = new VendaOrcamentoCabecalho();
		vendaOrcamentoCabecalho.setId(id);
		repository.delete(vendaOrcamentoCabecalho);
	}
	
}
