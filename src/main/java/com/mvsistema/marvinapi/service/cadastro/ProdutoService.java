package com.mvsistema.marvinapi.service.cadastro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.cadastro.Produto;
import com.mvsistema.marvinapi.repository.cadastro.ProdutoRepository;

@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository repository;
	
	public List<Produto> listaTodos() {
		return repository.findAll();
	}
	
	public List<Produto> listaTodos(String descricao) {
		return repository.findFirst10ByDescricaoContaining(descricao);
	}
	
	public Produto buscaPorId(Long id) {
		try {
			Produto produto = this.repository.findOne(id);
			if (produto == null) {
				throw new EmptyResultDataAccessException(1);
			}	
			return produto;
		} catch (NoSuchElementException | IllegalArgumentException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	public Produto salvaObjeto(Produto produto) {
		return repository.save(produto);
	}
	
	public void excluirObjeto(Long id) {
		Produto produto = new Produto();
		produto.setId(id);
		repository.delete(produto);
	}
	
}
