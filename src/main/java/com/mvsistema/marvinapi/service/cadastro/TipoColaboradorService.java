package com.mvsistema.marvinapi.service.cadastro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;

import com.mvsistema.marvinapi.model.cadastro.TipoColaborador;
import com.mvsistema.marvinapi.repository.cadastro.TipoColaboradorRepository;

@Service
public class TipoColaboradorService {

	@Autowired
	private TipoColaboradorRepository repository;
	
	public List<TipoColaborador> listaTodos() {
		return repository.findAll();
	}
	
	public List<TipoColaborador> listaTodos(String nome) {
		return repository.findFirst10ByNomeContaining(nome);
	}
	
	public TipoColaborador buscaPorId(Long id) {
		try {
			TipoColaborador TipoColaborador = this.repository.findOne(id);
			if (TipoColaborador == null) {
				throw new EmptyResultDataAccessException(1);
			}	
			return TipoColaborador;
		} catch (NoSuchElementException | IllegalArgumentException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	public TipoColaborador salvaObjeto(TipoColaborador tipoColaborador) {
		return repository.save(tipoColaborador);
	}
	
	public void excluirObjeto(Long id) {
		TipoColaborador tipoColaborador = new TipoColaborador();
		tipoColaborador.setId(id);
		repository.delete(tipoColaborador);
	}
	
}
