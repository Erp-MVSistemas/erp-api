package com.mvsistema.marvinapi.service.cadastro;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.cadastro.ContaCaixa;
import com.mvsistema.marvinapi.repository.cadastro.ContaCaixaRepository;

@Service
public class ContaCaixaService {

	@Autowired
	private ContaCaixaRepository repository;
	
	public List<ContaCaixa> listaTodos() {
		return repository.findAll();
	}
	
	public List<ContaCaixa> listaTodos(String nome) {
		return repository.findFirst10ByNomeContaining(nome);
	}
	
	public ContaCaixa buscaPorId(Long id) {
		try {
			ContaCaixa contaCaixa = this.repository.findOne(id);
			if (contaCaixa == null) {
				throw new EmptyResultDataAccessException(1);
			}	
			return contaCaixa;
		} catch (NoSuchElementException | IllegalArgumentException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	public ContaCaixa salvaObjeto(ContaCaixa contaCaixa) {
		return repository.save(contaCaixa);
	}
	
	public void excluirObjeto(Long id) {
		ContaCaixa contaCaixa = new ContaCaixa();
		contaCaixa.setId(id);
		repository.delete(contaCaixa);
	}
	
}
