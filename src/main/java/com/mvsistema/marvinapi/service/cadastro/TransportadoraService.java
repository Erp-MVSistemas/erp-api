package com.mvsistema.marvinapi.service.cadastro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.cadastro.Transportadora;
import com.mvsistema.marvinapi.repository.cadastro.TransportadoraRepository;

@Service
public class TransportadoraService {

	@Autowired
	private TransportadoraRepository repository;
	
	public List<Transportadora> listaTodos() {
		return repository.findAll();
	}

	public List<Transportadora> listaTodos(String nome) {
		return repository.findFirst10ByPessoaNomeContaining(nome);
	}
	
	public Transportadora buscaPorId(Long id) {
		try {
			Transportadora transportadora = this.repository.findOne(id);
			if (transportadora == null) {
				throw new EmptyResultDataAccessException(1);
			}	
			return transportadora;
		} catch (NoSuchElementException | IllegalArgumentException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}

	}
	
	public Transportadora salvaObjeto(Transportadora transportadora) {
		return repository.save(transportadora);
	}
	
	public void excluirObjeto(Long id) {
		Transportadora transportadora = new Transportadora();
		transportadora.setId(id);
		repository.delete(transportadora);
	}
	
}
