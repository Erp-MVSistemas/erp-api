package com.mvsistema.marvinapi.service.cadastro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.cadastro.Pessoa;
import com.mvsistema.marvinapi.model.cadastro.Setor;
import com.mvsistema.marvinapi.repository.cadastro.SetorRepository;

@Service
public class SetorService {

	@Autowired
	private SetorRepository repository;
	
	public List<Setor> listaTodos() {
		return repository.findAll();
	}
	
	public List<Setor> listaTodos(String nome) {
		return repository.findFirst10ByNomeContaining(nome);
	}
	
	public Setor buscaPorId(Long id) {
		try {
			Setor setor = this.repository.findOne(id);
			if (setor == null) {
				throw new EmptyResultDataAccessException(1);
			}	
			return setor;
		} catch (NoSuchElementException | IllegalArgumentException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	public Setor salvaObjeto(Setor setor) {
		return repository.save(setor);
	}
	
	public void excluirObjeto(Long id) {
		Setor setor = new Setor();
		setor.setId(id);
		repository.delete(setor);
	}
	
}
