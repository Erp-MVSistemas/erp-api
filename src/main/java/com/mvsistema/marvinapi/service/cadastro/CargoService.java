package com.mvsistema.marvinapi.service.cadastro;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.cadastro.Cargo;
import com.mvsistema.marvinapi.repository.cadastro.CargoRepository;

@Service
public class CargoService {

	@Autowired
	private CargoRepository repository;
	
	public List<Cargo> listaTodos() {
		return repository.findAll();
	}
	
	public List<Cargo> listaTodos(String nome) {
		return repository.findFirst10ByNomeContaining(nome);
	}
	
	public Cargo buscaPorId(Long id) {
		try {
			Cargo cargo = this.repository.findByIdCargo(id);
			if (cargo == null) {
				throw new EmptyResultDataAccessException(1);
			}	
			return cargo;
		} catch (NoSuchElementException | IllegalArgumentException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	public Cargo salvaObjeto(Cargo cargo) {
		return repository.save(cargo);
	}
	
	public void excluirObjeto(Long id) {
		Cargo cargo = new Cargo();
		cargo.setId(id);
		repository.delete(cargo);
	}
	
}
