package com.mvsistema.marvinapi.service.cadastro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.cadastro.Colaborador;
import com.mvsistema.marvinapi.model.cadastro.Pessoa;
import com.mvsistema.marvinapi.repository.cadastro.PessoaRepository;

@Service
public class PessoaService {

	@Autowired
	private PessoaRepository repository;
	
	public List<Pessoa> listaTodos() {
		return repository.findAll();
	}
	
	public List<Pessoa> listaTodos(String nome) {
		return repository.findFirst10ByNomeContaining(nome);
	}
	
	public Pessoa buscaPorId(Long id) {
		try {
			Pessoa pessoa = this.repository.findOne(id);
			if (pessoa == null) {
				throw new EmptyResultDataAccessException(1);
			}	
			return pessoa;
		} catch (NoSuchElementException | IllegalArgumentException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	public Pessoa salvaObjeto(Pessoa pessoa) {
		return repository.save(pessoa);
	}
	
	public void excluirObjeto(Long id) {
		Pessoa pessoa = new Pessoa();
		pessoa.setId(id);
		repository.delete(pessoa);
	}
	
}
