package com.mvsistema.marvinapi.service.cadastro;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.exception.UploadException;
import com.mvsistema.marvinapi.model.cadastro.Colaborador;
import com.mvsistema.marvinapi.repository.cadastro.ColaboradorRepository;

@Service
public class ColaboradorService {

	@Autowired
	private ColaboradorRepository repository;
	
	@Autowired
	private ServletContext context;

	public List<Colaborador> listaTodos() {
		return repository.findAll();
	}

	public List<Colaborador> listaTodos(String nome) {
		return repository.findFirst10ByPessoaNomeContaining(nome);
	}

	public Colaborador buscaPorId(Long id) {
		try {
			Colaborador colaborador = this.repository.findOne(id);
			if (colaborador == null) {
				throw new EmptyResultDataAccessException(1);
			}
			return colaborador;
		} catch (NoSuchElementException | IllegalArgumentException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}

	public Colaborador salvaObjeto(Colaborador colaborador) {
		return repository.save(colaborador);
	}

	public void excluirObjeto(Long id) {
		Colaborador colaborador = new Colaborador();
		colaborador.setId(id);
		repository.delete(colaborador);
	}

	public String uploadFoto(MultipartFile file, Long id) {
		try {
			String caminho = this.context.getRealPath("/");
			File pasta = new File(caminho + 
					System.getProperty("file.separator") + "images" + 
					System.getProperty("file.separator") + "colaborador" +
					System.getProperty("file.separator"));
			
			if (!pasta.exists()) {
				pasta.mkdirs();
			}
			System.out.println(pasta);
			File arquivo = new File(pasta.getAbsolutePath() + System.getProperty("file.separator") + id +".jpg");
			file.transferTo(arquivo);
			
			return System.getProperty("file.separator") + "images" + 
					System.getProperty("file.separator") + "colaborador" + 
					System.getProperty("file.separator") + id +".jpg";
			
//			if (arquivo.exists()) {
//				Colaborador colaborador = buscaPorId(id);
//				colaborador.setFoto34(System.getProperty("file.separator") + "images" + 
//						System.getProperty("file.separator") + "colaborador" + 
//						System.getProperty("file.separator") + id +".jpg");
//				salvaObjeto(colaborador);
//			}
		} catch (IllegalStateException | IOException e) {
			throw new UploadException(e.getMessage());
		}
	}

}
