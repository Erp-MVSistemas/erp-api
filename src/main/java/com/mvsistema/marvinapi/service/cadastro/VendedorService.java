package com.mvsistema.marvinapi.service.cadastro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.cadastro.TipoColaborador;
import com.mvsistema.marvinapi.model.cadastro.Vendedor;
import com.mvsistema.marvinapi.repository.cadastro.VendedorRepository;

@Service
public class VendedorService {

	@Autowired
	private VendedorRepository repository;
	
	public List<Vendedor> listaTodos() {
		return repository.findAll();
	}

	public List<Vendedor> listaTodos(String nome) {
		return repository.findFirst10ByColaboradorPessoaNomeContaining(nome);
	}
	
	public Vendedor buscaPorId(Long id) {
		
		try {
			Vendedor vendedor = this.repository.findOne(id);
			if (vendedor == null) {
				throw new EmptyResultDataAccessException(1);
			}	
			return vendedor;
		} catch (NoSuchElementException | IllegalArgumentException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}

	}
	
	public Vendedor salvaObjeto(Vendedor vendedor) {
		return repository.save(vendedor);
	}
	
	public void excluirObjeto(Long id) {
		Vendedor vendedor = new Vendedor();
		vendedor.setId(id);
		repository.delete(vendedor);
	}
	
}
