package com.mvsistema.marvinapi.service.cadastro;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.cadastro.Cliente;
import com.mvsistema.marvinapi.repository.cadastro.ClienteRepository;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository repository;
	
	public List<Cliente> listaTodos() {
		return repository.findAll();
	}
	
	public List<Cliente> listaTodos(String nome) {
		return repository.findFirst10ByPessoaNomeContaining(nome);
	}
	
	public Cliente buscaPorId(Long id) {
		try {
			Cliente cliente = this.repository.findOne(id);
			if (cliente == null) {
				throw new EmptyResultDataAccessException(1);
			}	
			return cliente;
		} catch (NoSuchElementException | IllegalArgumentException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}
	
	public Cliente salvaObjeto(Cliente cliente) {
		return repository.save(cliente);
	}
	
	public void excluirObjeto(Long id) {
		Cliente cliente = new Cliente();
		cliente.setId(id);
		repository.delete(cliente);
	}
	
}
