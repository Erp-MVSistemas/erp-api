package com.mvsistema.marvinapi.service.cadastro;

import java.util.List;
import java.util.NoSuchElementException;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.cadastro.Fornecedor;
import com.mvsistema.marvinapi.repository.cadastro.FornecedorRepository;

@Service
public class FornecedorService {

	@Autowired
	private FornecedorRepository repository;
	
	public List<Fornecedor> listaTodos() {
		return repository.findAll();
	}

	public List<Fornecedor> listaTodos(String nome) {
		return repository.findFirst10ByPessoaNomeContaining(nome);
	}

	public Fornecedor buscaPorId(Long id) {
		try {
			Fornecedor fornecedor = this.repository.findOne(id);
			if (fornecedor == null) {
				throw new EmptyResultDataAccessException(1);
			}
			return fornecedor;
		} catch (NoSuchElementException | IllegalArgumentException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}

	public Fornecedor salvaObjeto(Fornecedor fornecedor) {
		return repository.save(fornecedor);
	}

	public void excluirObjeto(Long id) {
		Fornecedor fornecedor = new Fornecedor();
		fornecedor.setId(id);
		repository.delete(fornecedor);
	}
}
