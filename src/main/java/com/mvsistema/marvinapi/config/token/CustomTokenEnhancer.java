package com.mvsistema.marvinapi.config.token;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import com.mvsistema.marvinapi.config.property.MarvinApiProperty;
import com.mvsistema.marvinapi.security.UsuarioSistema;

public class CustomTokenEnhancer implements TokenEnhancer {

	@Autowired
	private MarvinApiProperty marvinApiProperty;

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		UsuarioSistema usuarioSistema = (UsuarioSistema) authentication.getPrincipal();

		Map<String, Object> addInfo = new HashMap<>();
		addInfo.put("nome", usuarioSistema.getUsuario().getNome());
		addInfo.put("cpf", usuarioSistema.getUsuario().getCpf());
		addInfo.put("versaoApi", marvinApiProperty.getVersaoApi());
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(addInfo);

		return accessToken;
	}

}
