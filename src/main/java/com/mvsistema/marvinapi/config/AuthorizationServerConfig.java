package com.mvsistema.marvinapi.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import com.mvsistema.marvinapi.config.property.MarvinApiProperty;
import com.mvsistema.marvinapi.config.token.CustomTokenEnhancer;

/**
 * Classe responsavel pela liberaçao da autorizacao para acessar os recursos
 * @author viniciusluciene
 * MVSistema - Todos os Direitos reservados.
 * 14 de nov de 2018
 */
@Configuration
@EnableAuthorizationServer
@Profile("oauth-security")
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

	/**
	 * Responsavel para pegar o usuario e senha
	 */
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private MarvinApiProperty marvinApiProperty;
	
	/**
	 * O cliente aqui é a aplicaçao que esta requisitando.
	 */
	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients.inMemory()
			.withClient("angular")
			.secret("@ngul@r0")
			.scopes("read", "write")//aqui é limitado o acesso de leitura e escrita do cliente
		 	.authorizedGrantTypes("password", "refresh_token")//cliente vai receber usuario e senha e vai pegar o access token
		 	.accessTokenValiditySeconds(segundos())// 7min
		 	.refreshTokenValiditySeconds(3600 * 24);//quantos segundos esse token vai ficar ativo: 1800 / 60 = 30min
	}
	
	/**
	 * Metodo que recebe o autorizationserve endpoins
	 */
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		// Tokens Incrementados. Podendo colocarm mais dados no token para serem usados no front-end
		TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
		tokenEnhancerChain.setTokenEnhancers(Arrays.asList(tokenEnhancer(), accessTokenConverter()));
		
		
		endpoints.tokenStore(tokenStore())//a aplicacao vai pegar um token e depois ela manda de volta pra acessa a api
		.tokenEnhancer(tokenEnhancerChain)
		.reuseRefreshTokens(false) 
		.authenticationManager(authenticationManager);//para validar o usuario e senha
	}
	
	@Bean
	public TokenStore tokenStore() throws Exception {
		return new JwtTokenStore(accessTokenConverter());
	}
	
	@Bean
	public TokenEnhancer tokenEnhancer() {
	    return new CustomTokenEnhancer();
}
	
	
	private JwtAccessTokenConverter accessTokenConverter() throws Exception {
		JwtAccessTokenConverter accessTokenConverter = new JwtAccessTokenConverter();
		accessTokenConverter.setSigningKey("mvsistema");//palavra secreta do jwt
		accessTokenConverter.afterPropertiesSet();
		return accessTokenConverter;
	}
	
	private int segundos() {
		return marvinApiProperty.getTempoAcessoSegundos();
	}
	
}
