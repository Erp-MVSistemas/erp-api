package com.mvsistema.marvinapi.config.property;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

/**
 * classe para configuraçao do projeto: Profiles
 * 
 * @author viniciusluciene MVSistema - Todos os Direitos reservados. 26 de nov
 *         de 2018
 */
@ConfigurationProperties("marvinapi")
@Getter
@Setter
public class MarvinApiProperty {

	private String origemPermitida = "http://localhots:8080";
	private int tempoAcessoSegundos = 0;
	private String versaoApi;

	private final Seguranca Seguranca = new Seguranca();

	public static class Seguranca {
		@Getter
		@Setter
		private boolean enableHttps;

	}
}
