package com.mvsistema.marvinapi.repository.cadastro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mvsistema.marvinapi.model.cadastro.Cargo;

public interface CargoRepository extends JpaRepository<Cargo, Long> {
	
	@Query("SELECT c FROM Cargo c WHERE c.id = :id")
	public Cargo findByIdCargo(@Param("id") Long id);
	
	public List<Cargo> findFirst10ByNomeContaining(String nome);

}