package com.mvsistema.marvinapi.repository.cadastro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.mvsistema.marvinapi.model.cadastro.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {
	public List<Produto> findFirst10ByDescricaoContaining(String descricao);
}
