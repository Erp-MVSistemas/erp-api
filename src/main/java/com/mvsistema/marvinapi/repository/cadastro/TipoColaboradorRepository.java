package com.mvsistema.marvinapi.repository.cadastro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mvsistema.marvinapi.model.cadastro.TipoColaborador;

public interface TipoColaboradorRepository extends JpaRepository<TipoColaborador, Long> {

	public List<TipoColaborador> findFirst10ByNomeContaining(String nome);
}