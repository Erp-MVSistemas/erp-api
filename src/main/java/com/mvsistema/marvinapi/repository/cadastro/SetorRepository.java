package com.mvsistema.marvinapi.repository.cadastro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mvsistema.marvinapi.model.cadastro.Setor;

public interface SetorRepository extends JpaRepository<Setor, Long> {

	public List<Setor> findFirst10ByNomeContaining(String nome);

}