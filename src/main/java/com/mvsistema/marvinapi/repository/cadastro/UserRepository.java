package com.mvsistema.marvinapi.repository.cadastro;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mvsistema.marvinapi.model.cadastro.Users;

public interface UserRepository extends JpaRepository<Users, Long> {
	
	public Optional<Users> findByCpf(String cpf);
	public Optional<Users> findByCpfAndSenha(String cpf, String senha);
}
