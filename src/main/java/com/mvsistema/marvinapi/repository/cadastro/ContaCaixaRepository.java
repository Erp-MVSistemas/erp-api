package com.mvsistema.marvinapi.repository.cadastro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mvsistema.marvinapi.model.cadastro.Cargo;
import com.mvsistema.marvinapi.model.cadastro.ContaCaixa;

public interface ContaCaixaRepository extends JpaRepository<ContaCaixa, Long> {
		
	public List<ContaCaixa> findFirst10ByNomeContaining(String nome);

}