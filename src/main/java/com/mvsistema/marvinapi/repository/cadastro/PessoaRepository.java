package com.mvsistema.marvinapi.repository.cadastro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mvsistema.marvinapi.model.cadastro.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Long> {

	public List<Pessoa> findFirst10ByNomeContaining(String nome);

}