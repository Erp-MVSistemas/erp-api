package com.mvsistema.marvinapi.repository.cadastro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mvsistema.marvinapi.model.cadastro.Fornecedor;

public interface FornecedorRepository extends JpaRepository<Fornecedor, Long> {
	
	public List<Fornecedor> findFirst10ByPessoaNomeContaining(String nome);

}