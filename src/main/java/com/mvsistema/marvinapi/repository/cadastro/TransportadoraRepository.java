package com.mvsistema.marvinapi.repository.cadastro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mvsistema.marvinapi.model.cadastro.Transportadora;

public interface TransportadoraRepository extends JpaRepository<Transportadora, Long> {

	public List<Transportadora> findFirst10ByPessoaNomeContaining(String nome);
	
}