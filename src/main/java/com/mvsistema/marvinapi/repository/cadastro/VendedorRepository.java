package com.mvsistema.marvinapi.repository.cadastro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mvsistema.marvinapi.model.cadastro.Vendedor;


public interface VendedorRepository extends JpaRepository<Vendedor, Long> {

	public List<Vendedor> findFirst10ByColaboradorPessoaNomeContaining(String nome);
	
}