package com.mvsistema.marvinapi.repository.cadastro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mvsistema.marvinapi.model.cadastro.Colaborador;

public interface ColaboradorRepository extends JpaRepository<Colaborador, Long> {

	public List<Colaborador> findFirst10ByPessoaNomeContaining(String nome);
}