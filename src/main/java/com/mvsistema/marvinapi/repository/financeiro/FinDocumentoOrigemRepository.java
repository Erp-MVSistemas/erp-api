package com.mvsistema.marvinapi.repository.financeiro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mvsistema.marvinapi.model.financeiro.FinDocumentoOrigem;

public interface FinDocumentoOrigemRepository extends JpaRepository<FinDocumentoOrigem, Long> {

	public List<FinDocumentoOrigem> findFirst10BySiglaDocumentoContaining(String siglaDocumento);
}