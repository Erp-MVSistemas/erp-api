package com.mvsistema.marvinapi.repository.financeiro;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mvsistema.marvinapi.model.financeiro.FinParcelaPagamento;
import com.mvsistema.marvinapi.model.financeiro.FinParcelaPagar;

public interface FinParcelaPagamentoRepository extends JpaRepository<FinParcelaPagar, Long> {

}