package com.mvsistema.marvinapi.repository.financeiro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mvsistema.marvinapi.model.financeiro.FinStatusParcela;

public interface FinStatusParcelaRepository extends JpaRepository<FinStatusParcela, Long> {

	public List<FinStatusParcela> findFirst10ByDescricaoContaining(String descricao);
}