package com.mvsistema.marvinapi.repository.financeiro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mvsistema.marvinapi.model.financeiro.FinTipoPagamento;

public interface FinTipoPagamentoRepository extends JpaRepository<FinTipoPagamento, Long> {

	public List<FinTipoPagamento> findFirst10ByDescricaoContaining(String descricao);
}