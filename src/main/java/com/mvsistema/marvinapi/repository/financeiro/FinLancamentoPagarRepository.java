package com.mvsistema.marvinapi.repository.financeiro;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mvsistema.marvinapi.model.financeiro.FinLancamentoPagar;

public interface FinLancamentoPagarRepository extends JpaRepository<FinLancamentoPagar, Long> {

}