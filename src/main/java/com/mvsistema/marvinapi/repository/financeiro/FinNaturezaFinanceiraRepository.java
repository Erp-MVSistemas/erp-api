package com.mvsistema.marvinapi.repository.financeiro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mvsistema.marvinapi.model.financeiro.FinNaturezaFinanceira;

public interface FinNaturezaFinanceiraRepository extends JpaRepository<FinNaturezaFinanceira, Long> {

	public List<FinNaturezaFinanceira> findFirst10ByDescricaoContaining(String codigo);
}