package com.mvsistema.marvinapi.repository.compras;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mvsistema.marvinapi.model.compras.CompraRequisicao;
import com.mvsistema.marvinapi.model.compras.CompraRequisicaoDetalhe;

public interface CompraRequisicaoRepository extends JpaRepository<CompraRequisicao, Long> {

	@Query(value = "Select crd From CompraRequisicaoDetalhe crd")
	public List<CompraRequisicaoDetalhe> findAllListaCompraRequisicaoDetalhe();
}