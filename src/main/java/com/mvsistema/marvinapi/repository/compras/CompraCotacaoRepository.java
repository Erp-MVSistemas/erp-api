package com.mvsistema.marvinapi.repository.compras;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mvsistema.marvinapi.model.compras.CompraCotacao;

public interface CompraCotacaoRepository extends JpaRepository<CompraCotacao, Long> {

}