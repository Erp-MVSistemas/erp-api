package com.mvsistema.marvinapi.repository.compras;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mvsistema.marvinapi.model.compras.CompraTipoRequisicao;

public interface CompraTipoRequisicaoRepository extends JpaRepository<CompraTipoRequisicao, Long> {

	public List<CompraTipoRequisicao> findFirst10ByNomeContaining(String nome);

}