package com.mvsistema.marvinapi.repository.vendas;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mvsistema.marvinapi.model.vendas.VendaOrcamentoCabecalho;

public interface VendaOrcamentoCabecalhoRepository extends JpaRepository<VendaOrcamentoCabecalho, Long> {

	public List<VendaOrcamentoCabecalho> findFirst10ByCodigoContaining(String codigo);
}