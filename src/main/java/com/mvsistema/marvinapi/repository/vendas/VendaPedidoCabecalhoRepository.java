package com.mvsistema.marvinapi.repository.vendas;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mvsistema.marvinapi.model.vendas.VendaPedidoCabecalho;

public interface VendaPedidoCabecalhoRepository extends JpaRepository<VendaPedidoCabecalho, Long> {

	public List<VendaPedidoCabecalho> findFirst10ByNumeroFaturaContaining(Integer codigo);
}