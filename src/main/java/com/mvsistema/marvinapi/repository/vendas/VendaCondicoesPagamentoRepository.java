package com.mvsistema.marvinapi.repository.vendas;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mvsistema.marvinapi.model.vendas.VendaCondicoesPagamento;

public interface VendaCondicoesPagamentoRepository extends JpaRepository<VendaCondicoesPagamento, Long> {

	public List<VendaCondicoesPagamento> findFirst10ByNomeContaining(String nome);
}