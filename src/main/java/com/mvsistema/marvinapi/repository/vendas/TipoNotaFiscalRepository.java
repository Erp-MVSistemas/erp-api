package com.mvsistema.marvinapi.repository.vendas;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mvsistema.marvinapi.model.vendas.TipoNotaFiscal;

public interface TipoNotaFiscalRepository extends JpaRepository<TipoNotaFiscal, Long> {

	public List<TipoNotaFiscal> findFirst10ByModeloContaining(String nome);
}