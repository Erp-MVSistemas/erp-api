package com.mvsistema.marvinapi.repository.estoque;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mvsistema.marvinapi.model.estoque.EstoqueReajusteCabecalho;

public interface EstoqueReajusteCabecalhoRepository extends JpaRepository<EstoqueReajusteCabecalho, Long> {

	
}