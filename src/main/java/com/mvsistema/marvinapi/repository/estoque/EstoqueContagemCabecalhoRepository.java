package com.mvsistema.marvinapi.repository.estoque;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mvsistema.marvinapi.model.estoque.EstoqueContagemCabecalho;

public interface EstoqueContagemCabecalhoRepository extends JpaRepository<EstoqueContagemCabecalho, Long> {

	
}