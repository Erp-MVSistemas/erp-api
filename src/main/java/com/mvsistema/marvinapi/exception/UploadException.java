package com.mvsistema.marvinapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class UploadException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3103808671567434864L;
	
	public UploadException(String mensagem) {
		super(mensagem);
	}

}
