package com.mvsistema.marvinapi.resource.financeiro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.financeiro.FinTipoPagamento;
import com.mvsistema.marvinapi.service.financeiro.FinTipoPagamentoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/financeiro/tipo-pagamento")
@Api(value = "API Rest Fin. Tipo Pagamento")
public class FinTipoPagamentoResource {

	@Autowired
	private FinTipoPagamentoService service;

	@GetMapping
	@ApiOperation(value = "Retorna uma Fin. Tipo Pagamento")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<FinTipoPagamento> listaTodos() {
		return service.listaTodos();
	}

	@GetMapping("/lista/{nome}")
	@ApiOperation(value = "Retorna uma Fin. Tipo Pagamento por Nome")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<FinTipoPagamento> listaTodos(@PathVariable String nome) {
		return service.listaTodos(nome);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Fin. Tipo Pagamento por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public FinTipoPagamento buscaPorId(@PathVariable Long id) {
		try {
			return service.buscaPorId(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}

	@PostMapping
	@ApiOperation(value = "Salva uma Fin. Tipo Pagamento")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public FinTipoPagamento salvaObjeto(@RequestBody FinTipoPagamento finTipoPagamento) {
		return service.salvaObjeto(finTipoPagamento);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta um Fin. Tipo Pagamento pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		service.excluirObjeto(id);
	}

}
