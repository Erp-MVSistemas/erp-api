package com.mvsistema.marvinapi.resource.financeiro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.financeiro.FinDocumentoOrigem;
import com.mvsistema.marvinapi.service.financeiro.FinDocumentoOrigemService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/financeiro/documento-origem")
public class FinDocumentoOrigemResource {

	@Autowired
	private FinDocumentoOrigemService service;

	@GetMapping
	@ApiOperation(value = "Retorna uma Fin. Doc. Origem")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<FinDocumentoOrigem> listaTodos() {
		return service.listaTodos();
	}

	@GetMapping("/lista/{nome}")
	@ApiOperation(value = "Retorna uma Fin. Doc. Origem por Nome")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<FinDocumentoOrigem> listaTodos(@PathVariable String nome) {
		return service.listaTodos(nome);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Fin. Doc. Origem por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public FinDocumentoOrigem buscaPorId(@PathVariable Long id) {
		try {
			return service.buscaPorId(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}

	@PostMapping
	@ApiOperation(value = "Salva uma Fin. Doc. Origem")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public FinDocumentoOrigem salvaObjeto(@RequestBody FinDocumentoOrigem finDocumentoOrigem) {
		return service.salvaObjeto(finDocumentoOrigem);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta um Fin. Doc. Origem pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		service.excluirObjeto(id);
	}

}
