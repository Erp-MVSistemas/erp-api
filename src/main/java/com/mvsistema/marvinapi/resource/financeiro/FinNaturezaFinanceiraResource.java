package com.mvsistema.marvinapi.resource.financeiro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.financeiro.FinNaturezaFinanceira;
import com.mvsistema.marvinapi.service.financeiro.FinNaturezaFinanceiraService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/financeiro/natureza-financeira")
@Api(value = "API Rest Fin. Nat. Financeira")
public class FinNaturezaFinanceiraResource {

	@Autowired
	private FinNaturezaFinanceiraService service;

	@GetMapping
	@ApiOperation(value = "Retorna uma Fin. Nat. Financeira")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<FinNaturezaFinanceira> listaTodos() {
		return service.listaTodos();
	}

	@GetMapping("/lista/{nome}")
	@ApiOperation(value = "Retorna uma Fin. Nat. Financeira por Nome")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<FinNaturezaFinanceira> listaTodos(@PathVariable String nome) {
		return service.listaTodos(nome);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Fin. Nat. Financeira por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public FinNaturezaFinanceira buscaPorId(@PathVariable Long id) {
		try {
			return service.buscaPorId(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}

	@PostMapping
	@ApiOperation(value = "Salva uma Fin. Nat. Financeira")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public FinNaturezaFinanceira salvaObjeto(@RequestBody FinNaturezaFinanceira finNaturezaFinanceira) {
		return service.salvaObjeto(finNaturezaFinanceira);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta um Fin. Nat. Financeira pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		service.excluirObjeto(id);
	}

}
