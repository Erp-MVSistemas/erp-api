package com.mvsistema.marvinapi.resource.financeiro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.financeiro.FinLancamentoPagar;
import com.mvsistema.marvinapi.service.financeiro.FinLancamentoPagarService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/financeiro/lancamento-pagar")
@Api(value = "API Rest Fin. Lancamento Pagar")
public class FinLancamentoPagarResource {

	@Autowired
	private FinLancamentoPagarService service;

	@GetMapping
	@ApiOperation(value = "Retorna uma Fin. Lancamento Pagar")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<FinLancamentoPagar> listaTodos() {
		return service.listaTodos();
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Fin. Lancamento Pagar por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public FinLancamentoPagar buscaPorId(@PathVariable Long id) {
		try {
			return service.buscaPorId(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}

	@PostMapping
	@ApiOperation(value = "Salva uma Fin. Lancamento Pagar")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public FinLancamentoPagar salvaObjeto(@RequestBody FinLancamentoPagar finLancamentoPagar) {
		return service.salvaObjeto(finLancamentoPagar);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta um Fin. Lancamento Pagar pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		service.excluirObjeto(id);
	}

}
