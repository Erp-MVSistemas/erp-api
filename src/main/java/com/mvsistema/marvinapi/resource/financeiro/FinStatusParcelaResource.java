package com.mvsistema.marvinapi.resource.financeiro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.financeiro.FinStatusParcela;
import com.mvsistema.marvinapi.service.financeiro.FinStatusParcelaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/financeiro/status-parcela")
@Api(value = "API Rest Fin. Status Parcela")
public class FinStatusParcelaResource {

	@Autowired
	private FinStatusParcelaService service;

	@GetMapping
	@ApiOperation(value = "Retorna uma Fin. Status Parcela")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<FinStatusParcela> listaTodos() {
		return service.listaTodos();
	}

	@GetMapping("/lista/{nome}")
	@ApiOperation(value = "Retorna uma Fin. Status Parcela por Nome")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<FinStatusParcela> listaTodos(@PathVariable String nome) {
		return service.listaTodos(nome);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Fin. Status Parcela por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public FinStatusParcela buscaPorId(@PathVariable Long id) {
		try {
			return service.buscaPorId(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}

	@PostMapping
	@ApiOperation(value = "Salva uma Fin. Status Parcela")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public FinStatusParcela salvaObjeto(@RequestBody FinStatusParcela finStatusParcela) {
		return service.salvaObjeto(finStatusParcela);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta um Fin. Status Parcela pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		service.excluirObjeto(id);
	}

}
