package com.mvsistema.marvinapi.resource.financeiro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.financeiro.FinParcelaPagar;
import com.mvsistema.marvinapi.service.financeiro.FinParcelaPagamentoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/financeiro/parcela-pagamento")
@Api(value = "API Rest Fin. Parc. Pagamento")
public class FinParcelaPagamentoResource {

	@Autowired
	private FinParcelaPagamentoService service;

	@GetMapping
	@ApiOperation(value = "Retorna uma Fin. Parc. Pagamento")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<FinParcelaPagar> listaTodos() {
		return service.listaTodos();
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Fin. Parc. Pagamento por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public FinParcelaPagar buscaPorId(@PathVariable Long id) {
		try {
			return service.buscaPorId(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}

	@PostMapping
	@ApiOperation(value = "Salva uma Fin. Parc. Pagamento")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public FinParcelaPagar salvaObjeto(@RequestBody FinParcelaPagar finParcelaPagar) {
		return service.salvaObjeto(finParcelaPagar);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta um Fin. Parc. Pagamento pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		service.excluirObjeto(id);
	}

}
