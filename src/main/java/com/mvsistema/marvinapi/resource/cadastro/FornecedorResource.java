package com.mvsistema.marvinapi.resource.cadastro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mvsistema.marvinapi.model.cadastro.Fornecedor;
import com.mvsistema.marvinapi.service.cadastro.FornecedorService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping("/fornecedor")
@Api(value = "API Rest Fornecedor")
public class FornecedorResource {

	@Autowired
	private FornecedorService service;
	
	@GetMapping
	@ApiOperation(value = "Retorna uma Lista de Fornecedores")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<Fornecedor> listaTodos() {
		return this.service.listaTodos();
	}
	
	@GetMapping("/lista/{nome}")
	@ApiOperation(value = "Retorna uma Lista de Fornecedores por Nome")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<Fornecedor> listaTodos(@PathVariable String nome) {
		return this.service.listaTodos(nome);
	}
	
	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Lista de Fornecedores por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public Fornecedor buscaPorId(@PathVariable Long id) {
		return this.service.buscaPorId(id);
	}
	
	@PostMapping
	@ApiOperation(value = "Salva um Fornecedor")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public Fornecedor salvaObjeto(@RequestBody Fornecedor fornecedor) {
		return this.service.salvaObjeto(fornecedor);
	}
	
	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta um Fornecedor pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		this.service.excluirObjeto(id);
	}
}
