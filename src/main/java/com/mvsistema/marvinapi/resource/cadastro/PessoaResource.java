package com.mvsistema.marvinapi.resource.cadastro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvsistema.marvinapi.model.cadastro.Pessoa;
import com.mvsistema.marvinapi.service.cadastro.PessoaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/pessoa")
@Api(value = "API Rest Pessoa")
public class PessoaResource {

	@Autowired
	private PessoaService service;

	@GetMapping
	@ApiOperation(value = "Retorna uma Lista de Pessoas")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<Pessoa> listaTodos() {
		return this.service.listaTodos();
	}

	@GetMapping("/lista/{nome}")
	@ApiOperation(value = "Retorna uma Lista de Pessoas por Nome")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<Pessoa> listaTodos(@PathVariable String nome) {
		return this.service.listaTodos(nome);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Lista de Pessoas por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public Pessoa buscaPorId(@PathVariable Long id) {
		return this.service.buscaPorId(id);
	}

	@PostMapping
	@ApiOperation(value = "Salva um Pessoa")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public Pessoa salvaObjeto(@RequestBody Pessoa pessoa) {
		return this.service.salvaObjeto(pessoa);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta um Pessoa pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		this.service.excluirObjeto(id);
	}

}
