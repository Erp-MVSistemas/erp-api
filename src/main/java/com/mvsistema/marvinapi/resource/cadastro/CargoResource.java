package com.mvsistema.marvinapi.resource.cadastro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.cadastro.Cargo;
import com.mvsistema.marvinapi.service.cadastro.CargoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping("/cargo")
@Api(value = "API Rest Cargos")
public class CargoResource {

	@Autowired
	private CargoService service;
	
	@GetMapping
	@ApiOperation(value = "Retorna uma Lista de Cargos")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<Cargo> listaTodos() {
		return this.service.listaTodos();
	}
	
	@GetMapping("/lista/{nome}")
	@ApiOperation(value = "Retorna uma Lista de Cargos por Nome")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<Cargo> listaTodos(@PathVariable String nome) {
		return this.service.listaTodos(nome);
	}
	
	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Lista de Cargos por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public Cargo buscaPorId(@PathVariable Long id) {
		return this.service.buscaPorId(id);
	}
	
	@PostMapping
	@ApiOperation(value = "Salva um Cargo")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public Cargo salvaObjeto(@RequestBody Cargo cargo) {
		return this.service.salvaObjeto(cargo);
	}
	
	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta um Cargo pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		this.service.excluirObjeto(id);
	}
	
}
