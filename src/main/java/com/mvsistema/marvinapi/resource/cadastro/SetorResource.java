package com.mvsistema.marvinapi.resource.cadastro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.cadastro.Setor;
import com.mvsistema.marvinapi.service.cadastro.SetorService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/setor")
@Api(value = "API Rest Setor")
public class SetorResource {

	@Autowired
	private SetorService service;

	@GetMapping
	@ApiOperation(value = "Retorna uma Lista de Setores")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<Setor> listaTodos() {
		return service.listaTodos();
	}

	@GetMapping("/lista/{nome}")
	@ApiOperation(value = "Retorna uma Lista de Setores por Nome")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<Setor> listaTodos(@PathVariable String nome) {
		return service.listaTodos(nome);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Lista de Setores por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public Setor buscaPorId(@PathVariable Long id) {
		try {
			return service.buscaPorId(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}

	@PostMapping
	@ApiOperation(value = "Salva uma Setores")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public Setor salvaObjeto(@RequestBody Setor setor) {
		return service.salvaObjeto(setor);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta um Setores pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		service.excluirObjeto(id);
	}

}
