package com.mvsistema.marvinapi.resource.cadastro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.cadastro.TipoColaborador;
import com.mvsistema.marvinapi.service.cadastro.TipoColaboradorService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/tipocolaborador")
@Api(value = "API Rest Tipo Colaborador")
public class TipoColaboradorResource {

	@Autowired
	private TipoColaboradorService service;

	@GetMapping
	@ApiOperation(value = "Retorna uma Lista de Tipo Colaborador")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<TipoColaborador> listaTodos() {
		return service.listaTodos();
	}

	@GetMapping("/lista/{nome}")
	@ApiOperation(value = "Retorna uma Lista de Tipo Colaborador por Nome")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<TipoColaborador> listaTodos(@PathVariable String nome) {
		return service.listaTodos(nome);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Lista de Tipo Colaborador por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public TipoColaborador buscaPorId(@PathVariable Long id) {
		try {
			return service.buscaPorId(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}

	@PostMapping
	@ApiOperation(value = "Salva uma Tipo Colaborador")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public TipoColaborador salvaObjeto(@RequestBody TipoColaborador tipoColaborador) {
		return service.salvaObjeto(tipoColaborador);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta um Tipo Colaborador pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		service.excluirObjeto(id);
	}

}
