package com.mvsistema.marvinapi.resource.cadastro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvsistema.marvinapi.model.cadastro.ContaCaixa;
import com.mvsistema.marvinapi.service.cadastro.ContaCaixaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping("/conta-caixa")
@Api(value = "API Rest Conta Caixa")
public class ContaCaixaResource {

	@Autowired
	private ContaCaixaService service;
	
	@GetMapping
	@ApiOperation(value = "Retorna uma Lista de Contas Caixa")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<ContaCaixa> listaTodos() {
		return this.service.listaTodos();
	}
	
	@GetMapping("/lista/{nome}")
	@ApiOperation(value = "Retorna uma Lista de Contas Caixa por Nome")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<ContaCaixa> listaTodos(@PathVariable String nome) {
		return this.service.listaTodos(nome);
	}
	
	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Lista de Conta Caixa por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public ContaCaixa buscaPorId(@PathVariable Long id) {
		return this.service.buscaPorId(id);
	}
	
	@PostMapping
	@ApiOperation(value = "Salva uma Conta Caixa")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public ContaCaixa salvaObjeto(@RequestBody ContaCaixa contaCaixa) {
		return this.service.salvaObjeto(contaCaixa);
	}
	
	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta uma Conta Caixa pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		this.service.excluirObjeto(id);
	}
	
}
