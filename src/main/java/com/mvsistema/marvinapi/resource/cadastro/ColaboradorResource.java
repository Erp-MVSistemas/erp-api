package com.mvsistema.marvinapi.resource.cadastro;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mvsistema.marvinapi.model.cadastro.Colaborador;
import com.mvsistema.marvinapi.service.cadastro.ColaboradorService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping("/colaborador")
@Api(value = "API Rest Colaborador")
public class ColaboradorResource {

	@Autowired
	private ColaboradorService service;
	
	@GetMapping
	@ApiOperation(value = "Retorna uma Lista de Colaboradores")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<Colaborador> listaTodos() {
		return this.service.listaTodos();
	}
	
	@GetMapping("/lista/{nome}")
	@ApiOperation(value = "Retorna uma Lista de Colaboradores por Nome")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<Colaborador> listaTodos(@PathVariable String nome) {
		return this.service.listaTodos(nome);
	}
	
	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Lista de Colaboradores por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public Colaborador buscaPorId(@PathVariable Long id) {
		return this.service.buscaPorId(id);
	}
	
	@PostMapping
	@ApiOperation(value = "Salva um Colaborador")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public Colaborador salvaObjeto(@RequestBody Colaborador colaborador) {
		return this.service.salvaObjeto(colaborador);
	}
	
	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta um Colaborador pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		this.service.excluirObjeto(id);
	}
	
//	@PostMapping("/upload/{id}")
//	public void uploadFoto(@RequestParam MultipartFile file, @PathVariable Long id) {
//		this.service.uploadFoto(file, id);
//	}
	
	@PostMapping("/upload/{id}")
	@ApiOperation(value = "Salva imagem de um Colaborador")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public String uploadFoto(@RequestParam MultipartFile file, @PathVariable Long id) {
		return this.service.uploadFoto(file, id);
	}
	
}
