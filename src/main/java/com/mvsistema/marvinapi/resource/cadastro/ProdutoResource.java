package com.mvsistema.marvinapi.resource.cadastro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.cadastro.Produto;
import com.mvsistema.marvinapi.service.cadastro.ProdutoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping("/produto")
@Api(value = "API Rest Produto")
public class ProdutoResource {

	@Autowired
	private ProdutoService service;
	

	  @GetMapping
	  @ApiOperation(value = "Retorna uma Produto")
	  @PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	  public List<Produto> listaTodos() {
	    return service.listaTodos();
	  }

	  @GetMapping("/lista/{nome}")
	  @ApiOperation(value = "Retorna uma Produto por Nome")
	  @PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	  public List<Produto> listaTodos(@PathVariable String nome) {
	    return service.listaTodos(nome);
	  }
	  
	  @GetMapping("/{id}")
	  @ApiOperation(value = "Retorna uma Produto por ID")
	  @PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	  public Produto buscaPorId(@PathVariable Long id) {
	    try {
	      return service.buscaPorId(id);
	    } catch (NoSuchElementException e) {
	      throw new RecursoNaoEncontradoException("Registro não localizado.");
	    }
	  }
	  
	  @PostMapping
	  @ApiOperation(value = "Salva uma Produto")
	  @PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	  public Produto salvaObjeto(@RequestBody Produto produto) {
	    return service.salvaObjeto(produto);
	  }
	  
	  @DeleteMapping("/{id}")
	  @ApiOperation(value = "Deleta um Produto pelo ID")
	  @PreAuthorize("hasAuthority('ROLE_REMOVER')")
	  public void excluirObjeto(@PathVariable Long id) {
	    service.excluirObjeto(id);
	  }
	
}
