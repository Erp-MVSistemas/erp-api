package com.mvsistema.marvinapi.resource.cadastro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvsistema.marvinapi.model.cadastro.Cliente;
import com.mvsistema.marvinapi.service.cadastro.ClienteService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/cliente")
@Api(value = "API Rest Cliente")
public class ClienteResource {

	@Autowired
	private ClienteService service;

	@GetMapping
	@ApiOperation(value = "Retorna uma Lista de Clientes")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<Cliente> listaTodos() {
		return this.service.listaTodos();
	}

	@GetMapping("/lista/{nome}")
	@ApiOperation(value = "Retorna uma Lista de Clientes por Nome")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<Cliente> listaTodos(@PathVariable String nome) {
		return this.service.listaTodos(nome);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Lista de Clientes por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public Cliente buscaPorId(@PathVariable Long id) {
		return this.service.buscaPorId(id);
	}

	@PostMapping
	@ApiOperation(value = "Salva um Cliente")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public Cliente salvaObjeto(@RequestBody Cliente cliente) {
		return this.service.salvaObjeto(cliente);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta um Cliente pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		this.service.excluirObjeto(id);
	}

}
