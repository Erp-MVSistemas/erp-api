package com.mvsistema.marvinapi.resource.cadastro;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.cadastro.Transportadora;
import com.mvsistema.marvinapi.service.cadastro.TransportadoraService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/transportadora")
@Api(value = "API Rest Transportadora")
public class TransportadoraController {

	@Autowired
	private TransportadoraService service;

	@GetMapping
	@ApiOperation(value = "Retorna uma Lista de Transportadora")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<Transportadora> listaTodos() {
		return service.listaTodos();
	}

	@GetMapping("/lista/{nome}")
	@ApiOperation(value = "Retorna uma Lista de Transportadora por Nome")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<Transportadora> listaTodos(@PathVariable String nome) {
		return service.listaTodos(nome);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Lista de Transportadora por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public Transportadora buscaPorId(@PathVariable Long id) {
		try {
			return service.buscaPorId(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}

	@PostMapping
	@ApiOperation(value = "Salva uma Transportadora")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public Transportadora salvaObjeto(@RequestBody Transportadora transportadora) {
		return service.salvaObjeto(transportadora);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta um Transportadora pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		service.excluirObjeto(id);
	}

}
