package com.mvsistema.marvinapi.resource.cadastro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvsistema.marvinapi.model.cadastro.Vendedor;
import com.mvsistema.marvinapi.service.cadastro.VendedorService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/vendedor")
@Api(value = "API Rest Vendedores")
public class VendedorController {

	@Autowired
	private VendedorService service;

	@GetMapping
	@ApiOperation(value = "Retorna uma Lista de Vendedores")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<Vendedor> listaTodos() {
		return this.service.listaTodos();
	}

	@GetMapping("/lista/{nome}")
	@ApiOperation(value = "Retorna uma Lista de Vendedores por Nome")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<Vendedor> listaTodos(@PathVariable String nome) {
		return this.service.listaTodos(nome);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Lista de Vendedores por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public Vendedor buscaPorId(@PathVariable Long id) {
		return this.service.buscaPorId(id);
	}

	@PostMapping
	@ApiOperation(value = "Salva um Vendedor")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public Vendedor salvaObjeto(@RequestBody Vendedor vendedor) {
		return this.service.salvaObjeto(vendedor);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta um Vendedor pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		this.service.excluirObjeto(id);
	}

}
