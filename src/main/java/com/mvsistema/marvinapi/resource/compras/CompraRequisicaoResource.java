package com.mvsistema.marvinapi.resource.compras;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.compras.CompraRequisicao;
import com.mvsistema.marvinapi.service.compras.CompraRequisicaoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/compra/requisicao")
@Api(value = "API Rest Compra Requisicao")
public class CompraRequisicaoResource {

	@Autowired
	private CompraRequisicaoService service;

	@GetMapping
	@ApiOperation(value = "Retorna uma Compra Requisicao")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<CompraRequisicao> listaTodos() {
		return service.listaTodos();
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Compra Requisicao por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public CompraRequisicao buscaPorId(@PathVariable Long id) {
		try {
			return service.buscaPorId(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}

	@PostMapping
	@ApiOperation(value = "Salva uma Compra Requisicao")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public CompraRequisicao salvaObjeto(@RequestBody CompraRequisicao compraRequisicao) {
		return service.salvaObjeto(compraRequisicao);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta um Compra Requisicao pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		service.excluirObjeto(id);
	}

}
