package com.mvsistema.marvinapi.resource.compras;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.compras.CompraTipoRequisicao;
import com.mvsistema.marvinapi.service.compras.CompraTipoRequisicaoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/compra/tipo-requisicao")
@Api(value = "API Rest Compra Tipo Requisicao")
public class CompraTipoRequisicaoResource {

	@Autowired
	private CompraTipoRequisicaoService service;

	@GetMapping
	@ApiOperation(value = "Retorna uma Compra Tipo Requisicao")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<CompraTipoRequisicao> listaTodos() {
		return service.listaTodos();
	}

	@GetMapping("/lista/{nome}")
	@ApiOperation(value = "Retorna uma Compra Tipo Requisicao por Nome")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<CompraTipoRequisicao> listaTodos(@PathVariable String nome) {
		return service.listaTodos(nome);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Compra Tipo Requisicao por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public CompraTipoRequisicao buscaPorId(@PathVariable Long id) {
		try {
			return service.buscaPorId(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}

	@PostMapping
	@ApiOperation(value = "Salva uma Compra Tipo Requisicao")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public CompraTipoRequisicao salvaObjeto(@RequestBody CompraTipoRequisicao compraTipoRequisicao) {
		return service.salvaObjeto(compraTipoRequisicao);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta um Compra Tipo Requisicao pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		service.excluirObjeto(id);
	}

}
