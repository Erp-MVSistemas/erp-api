package com.mvsistema.marvinapi.resource.compras;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.compras.CompraCotacao;
import com.mvsistema.marvinapi.service.compras.CompraCotacaoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/compra/cotacao")
@Api(value = "API Rest Compra Cotacao")
public class CompraCotacaoResource {

	@Autowired
	private CompraCotacaoService service;

	@GetMapping
	@ApiOperation(value = "Retorna uma Compra Cotacao")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<CompraCotacao> listaTodos() {
		return service.listaTodos();
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Compra Cotacao por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public CompraCotacao buscaPorId(@PathVariable Long id) {
		try {
			return service.buscaPorId(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}

	@PostMapping
	@ApiOperation(value = "Salva uma Compra Cotacao")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public CompraCotacao salvaObjeto(@RequestBody CompraCotacao compraCotacao) {
		return service.salvaObjeto(compraCotacao);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta um Compra Cotacao pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		service.excluirObjeto(id);
	}

}
