package com.mvsistema.marvinapi.resource.vendas;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.vendas.VendaCondicoesPagamento;
import com.mvsistema.marvinapi.service.vendas.VendaCondicoesPagamentoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/vendas/condicoes-pagamento")
@Api(value = "API Rest Condicao de Pagamento")
public class VendaCondicoesPagamentoResource {

	@Autowired
	private VendaCondicoesPagamentoService service;

	@GetMapping
	@ApiOperation(value = "Retorna uma Condicao de Pagamento")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<VendaCondicoesPagamento> listaTodos() {
		return service.listaTodos();
	}

	@GetMapping("/lista/{nome}")
	@ApiOperation(value = "Retorna uma Condicao de Pagamento por Nome")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<VendaCondicoesPagamento> listaTodos(@PathVariable String nome) {
		return service.listaTodos(nome);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Condicao de Pagamento por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public VendaCondicoesPagamento buscaPorId(@PathVariable Long id) {
		try {
			return service.buscaPorId(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}

	@PostMapping
	@ApiOperation(value = "Salva uma Condicao de Pagamento")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public VendaCondicoesPagamento salvaObjeto(@RequestBody VendaCondicoesPagamento vendaCondicoesPagamento) {
		return service.salvaObjeto(vendaCondicoesPagamento);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta um Condicao de Pagamento pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		service.excluirObjeto(id);
	}

}
