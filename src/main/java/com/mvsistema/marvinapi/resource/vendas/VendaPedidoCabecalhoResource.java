package com.mvsistema.marvinapi.resource.vendas;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.vendas.VendaPedidoCabecalho;
import com.mvsistema.marvinapi.service.vendas.VendaPedidoCabecalhoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/vendas/pedido")
@Api(value = "API Rest Venda Pedido")
public class VendaPedidoCabecalhoResource {

	@Autowired
	private VendaPedidoCabecalhoService service;

	@GetMapping
	@ApiOperation(value = "Retorna uma Venda Pedido")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<VendaPedidoCabecalho> listaTodos() {
		return service.listaTodos();
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Venda Pedido por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public VendaPedidoCabecalho buscaPorId(@PathVariable Long id) {
		try {
			return service.buscaPorId(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}

	@PostMapping
	@ApiOperation(value = "Salva uma Venda Pedido")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public VendaPedidoCabecalho salvaObjeto(@RequestBody VendaPedidoCabecalho vendaPedidoCabecalho) {
		return service.salvaObjeto(vendaPedidoCabecalho);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta um Venda Pedido pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		service.excluirObjeto(id);
	}

}
