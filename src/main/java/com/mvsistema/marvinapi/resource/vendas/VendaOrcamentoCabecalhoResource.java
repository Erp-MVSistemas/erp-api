package com.mvsistema.marvinapi.resource.vendas;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.vendas.VendaOrcamentoCabecalho;
import com.mvsistema.marvinapi.service.vendas.VendaOrcamentoCabecalhoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/vendas/orcamento")
@Api(value = "API Rest Venda Orcamento")
public class VendaOrcamentoCabecalhoResource {

	@Autowired
	private VendaOrcamentoCabecalhoService service;

	@GetMapping
	@ApiOperation(value = "Retorna uma Venda Orcamento")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<VendaOrcamentoCabecalho> listaTodos() {
		return service.listaTodos();
	}

	@GetMapping("/lista/{nome}")
	@ApiOperation(value = "Retorna uma Venda Orcamento por Nome")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<VendaOrcamentoCabecalho> listaTodos(@PathVariable String nome) {
		return service.listaTodos(nome);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Venda Orcamento por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public VendaOrcamentoCabecalho buscaPorId(@PathVariable Long id) {
		try {
			return service.buscaPorId(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}

	@PostMapping
	@ApiOperation(value = "Salva uma Venda Orcamento")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public VendaOrcamentoCabecalho salvaObjeto(@RequestBody VendaOrcamentoCabecalho vendaOrcamentoCabecalho) {
		return service.salvaObjeto(vendaOrcamentoCabecalho);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta um Venda Orcamento pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		service.excluirObjeto(id);
	}

}
