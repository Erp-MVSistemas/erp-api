package com.mvsistema.marvinapi.resource.vendas;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.vendas.TipoNotaFiscal;
import com.mvsistema.marvinapi.service.vendas.TipoNotaFiscalService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/vendas/tipo-nota-fiscal")
@Api(value = "API Rest Tipo Nota Fiscal")
public class TipoNotaFiscalResource {

	@Autowired
	private TipoNotaFiscalService service;

	@GetMapping
	@ApiOperation(value = "Retorna uma Tipo Nota Fiscal")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<TipoNotaFiscal> listaTodos() {
		return service.listaTodos();
	}

	@GetMapping("/lista/{nome}")
	@ApiOperation(value = "Retorna uma Tipo Nota Fiscal por Nome")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<TipoNotaFiscal> listaTodos(@PathVariable String nome) {
		return service.listaTodos(nome);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Tipo Nota Fiscal por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public TipoNotaFiscal buscaPorId(@PathVariable Long id) {
		try {
			return service.buscaPorId(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}

	@PostMapping
	@ApiOperation(value = "Salva uma Tipo Nota Fiscal")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public TipoNotaFiscal salvaObjeto(@RequestBody TipoNotaFiscal tipoNotaFiscal) {
		return service.salvaObjeto(tipoNotaFiscal);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta um Tipo Nota Fiscal pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		service.excluirObjeto(id);
	}

}
