package com.mvsistema.marvinapi.resource.estoque;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.estoque.EstoqueReajusteCabecalho;
import com.mvsistema.marvinapi.service.estoque.EstoqueReajusteCabecalhoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/estoque/reajuste")
@Api(value = "API Rest Estoque Reajuste")
public class EstoqueReajusteCabecalhoResource {

	@Autowired
	private EstoqueReajusteCabecalhoService service;

	@GetMapping
	@ApiOperation(value = "Retorna uma Estoque Reajuste")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<EstoqueReajusteCabecalho> listaTodos() {
		return service.listaTodos();
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Estoque Reajuste por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public EstoqueReajusteCabecalho buscaPorId(@PathVariable Long id) {
		try {
			return service.buscaPorId(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}

	@PostMapping
	@ApiOperation(value = "Salva uma Estoque Reajuste")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public EstoqueReajusteCabecalho salvaObjeto(@RequestBody EstoqueReajusteCabecalho estoqueReajusteCabecalho) {
		return service.salvaObjeto(estoqueReajusteCabecalho);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta um Estoque Reajuste pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		service.excluirObjeto(id);
	}

}
