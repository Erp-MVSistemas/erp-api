package com.mvsistema.marvinapi.resource.estoque;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mvsistema.marvinapi.exception.RecursoNaoEncontradoException;
import com.mvsistema.marvinapi.model.estoque.EstoqueContagemCabecalho;
import com.mvsistema.marvinapi.service.estoque.EstoqueContagemCabecalhoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/estoque/contagem")
@Api(value = "API Rest Estoque Contagem")
public class EstoqueContagemCabecalhoResource {

	@Autowired
	private EstoqueContagemCabecalhoService service;

	@GetMapping
	@ApiOperation(value = "Retorna uma Estoque Contagem")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public List<EstoqueContagemCabecalho> listaTodos() {
		return service.listaTodos();
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Retorna uma Estoque Contagem por ID")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR')")
	public EstoqueContagemCabecalho buscaPorId(@PathVariable Long id) {
		try {
			return service.buscaPorId(id);
		} catch (NoSuchElementException e) {
			throw new RecursoNaoEncontradoException("Registro não localizado.");
		}
	}

	@PostMapping
	@ApiOperation(value = "Salva uma Estoque Contagem")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR')")
	public EstoqueContagemCabecalho salvaObjeto(@RequestBody EstoqueContagemCabecalho estoqueContagemCabecalho) {
		return service.salvaObjeto(estoqueContagemCabecalho);
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value = "Deleta um Estoque Contagem pelo ID")
	@PreAuthorize("hasAuthority('ROLE_REMOVER')")
	public void excluirObjeto(@PathVariable Long id) {
		service.excluirObjeto(id);
	}

}
