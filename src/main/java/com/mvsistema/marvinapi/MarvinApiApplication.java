package com.mvsistema.marvinapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.mvsistema.marvinapi.config.property.MarvinApiProperty;

@SpringBootApplication
@EnableConfigurationProperties(MarvinApiProperty.class )
public class MarvinApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarvinApiApplication.class, args);
		geraSenha();
	}
	
	private static void geraSenha() {
		System.out.println(new BCryptPasswordEncoder().encode("123"));
	}
}
