package com.mvsistema.marvinapi.security;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.mvsistema.marvinapi.model.cadastro.Users;
import com.mvsistema.marvinapi.repository.cadastro.UserRepository;

/**
 * Classe responsavel por pegar o usuario do banco para implementar
 * na configuração ResourceServerConfig
 * @author viniciusluciene
 * MVSistema - Todos os Direitos reservados.
 * 26 de nov de 2018
 */
@Service
public class AppUserDetailsService implements UserDetailsService{
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String cpf) throws UsernameNotFoundException {
		Optional<Users> usuarioOptional = userRepository.findByCpf(cpf);
		Users usuario = usuarioOptional.orElseThrow(() -> new UsernameNotFoundException("Cpf e/ou Senha Invalido!"));
		return new UsuarioSistema(usuario, getPermissoes(usuario));
	}

	private Collection<? extends GrantedAuthority> getPermissoes(Users usuario) {
		Set<SimpleGrantedAuthority> permissoes = new HashSet<>();
		usuario.getPermissoes().forEach(p -> permissoes.add(new SimpleGrantedAuthority(p.getDescricao().toUpperCase())));
		return permissoes;
	}

}
