package com.mvsistema.marvinapi.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.mvsistema.marvinapi.model.cadastro.Users;

import lombok.Getter;

public class UsuarioSistema extends User{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5965568150957169076L;
	
	@Getter
	private Users usuario;

	public UsuarioSistema(Users usuario, Collection<? extends GrantedAuthority> authorities) {
		super(usuario.getCpf(), usuario.getSenha(), authorities);
		this.usuario = usuario;
	}
}
