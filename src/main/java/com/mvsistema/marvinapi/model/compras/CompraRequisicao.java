package com.mvsistema.marvinapi.model.compras;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.mvsistema.marvinapi.model.cadastro.Colaborador;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="compra_requisicao")
@Getter
@Setter
public class CompraRequisicao implements Serializable {
	
	private static final long serialVersionUID = -4818073604098503706L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_compra_requisicao")
	private Long id;

	private LocalDate dataRequisicao;

	@ManyToOne
	@JoinColumn(name="id_compra_tipo_requisicao")
	private CompraTipoRequisicao compraTipoRequisicao = new CompraTipoRequisicao();

	@ManyToOne
	@JoinColumn(name="id_colaborador")
	private Colaborador colaborador = new Colaborador();
	
	@OneToMany(mappedBy = "compraRequisicao", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<CompraRequisicaoDetalhe> listaCompraRequisicaoDetalhe = new ArrayList<CompraRequisicaoDetalhe>();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CompraRequisicao other = (CompraRequisicao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}