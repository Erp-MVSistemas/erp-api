package com.mvsistema.marvinapi.model.compras;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Entity
@Table(name="compra_cotacao_pedido_detalhe")
@Getter
@Setter
public class CompraCotacaoPedidoDetalhe implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -1620521267801139796L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_compra_cot_ped_det")
	private Long id;

	@Column(precision = 19, scale = 8)
	private BigDecimal quantidadePedida;

	@ManyToOne
	@JoinColumn(name="id_compra_cot_det")
	private CompraCotacaoDetalhe compraCotacaoDetalhe;

	@ManyToOne
	@JoinColumn(name="id_compra_pedido")
	private CompraPedido compraPedido;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CompraCotacaoPedidoDetalhe other = (CompraCotacaoPedidoDetalhe) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}