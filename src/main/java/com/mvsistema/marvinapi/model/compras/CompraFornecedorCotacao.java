package com.mvsistema.marvinapi.model.compras;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mvsistema.marvinapi.model.cadastro.Fornecedor;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="compra_fornecedor_cotacao")
@Getter
@Setter
public class CompraFornecedorCotacao implements Serializable {
	
	private static final long serialVersionUID = -8761704631414063068L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_compra_forn_cot")
	private Long id;

	private String prazoEntrega;

	@Column(precision = 19, scale = 8)
	private BigDecimal taxaDesconto;

	@Column(precision = 19, scale = 8)
	private BigDecimal total;

	@Column(precision = 19, scale = 8)
	private BigDecimal valorDesconto;

	@Column(precision = 19, scale = 8)
	private BigDecimal valorSubtotal;

	private String vendaCondicoesPagamento;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="id_compra_cotacao")
	private CompraCotacao compraCotacao;

	@ManyToOne
	@JoinColumn(name="id_fornecedor")
	private Fornecedor fornecedor;
	
	@OneToMany(mappedBy = "compraFornecedorCotacao",cascade = CascadeType.ALL, orphanRemoval = true)
	private List<CompraCotacaoDetalhe> listaCompraCotacaoDetalhe = new ArrayList<CompraCotacaoDetalhe>();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CompraFornecedorCotacao other = (CompraFornecedorCotacao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}