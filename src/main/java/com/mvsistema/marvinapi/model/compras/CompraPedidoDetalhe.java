package com.mvsistema.marvinapi.model.compras;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.mvsistema.marvinapi.model.cadastro.Produto;

import lombok.Getter;
import lombok.Setter;


/**
 * The persistent class for the compra_pedido_detalhe database table.
 * 
 */
@Entity
@Table(name="compra_pedido_detalhe")
@Getter
@Setter
public class CompraPedidoDetalhe implements Serializable {
	
	private static final long serialVersionUID = -1191174311335686178L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_compra_pedido_detalhe")
	private Long id;

	@Column(precision = 19, scale = 8)
	private BigDecimal quantidade;

	@Column(precision = 19, scale = 8)
	private BigDecimal taxaDesconto;

	@Column(precision = 19, scale = 8)
	private BigDecimal valorDesconto;

	@Column(precision = 19, scale = 8)
	private BigDecimal valorSubtotal;

	@Column(precision = 19, scale = 8)
	private BigDecimal valorTotal;

	@Column(precision = 19, scale = 8)
	private BigDecimal valorUnitario;

	@ManyToOne
	@JoinColumn(name="id_compra_pedido")
	private CompraPedido compraPedido;

	@ManyToOne
	@JoinColumn(name="id_produto")
	private Produto produto;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CompraPedidoDetalhe other = (CompraPedidoDetalhe) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}