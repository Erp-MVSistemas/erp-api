package com.mvsistema.marvinapi.model.compras;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.mvsistema.marvinapi.model.cadastro.Fornecedor;

import lombok.Getter;
import lombok.Setter;


/**
 * The persistent class for the compra_pedido database table.
 * 
 */
@Entity
@Table(name="compra_pedido")
@Getter
@Setter
public class CompraPedido implements Serializable {
	
	private static final long serialVersionUID = 3558344455979028702L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_compra_pedido")
	private Long id;

	private String contato;

	private LocalDate dataPedido;

	private LocalDate dataPrevisaoPagamento;

	private LocalDate dataPrevistaEntrega;

	private int diasIntervalo;

	private int diasPrimeiroVencimento;

	private String formaPagamento;

	private String localCobranca;

	private String localEntrega;

	private int quantidadeParcelas;

	@Column(precision = 19, scale = 8)
	private BigDecimal taxaDesconto;

	private String tipoFrete;

	@Column(precision = 19, scale = 8)
	private BigDecimal valorDesconto;

	@Column(precision = 19, scale = 8)
	private BigDecimal valorSubtotal;

	@Column(precision = 19, scale = 8)
	private BigDecimal valorTotalPedido;

	@ManyToOne
	@JoinColumn(name="id_compra_tipo_pedido")
	private CompraTipoPedido compraTipoPedido;

	@ManyToOne
	@JoinColumn(name="id_fornecedor")
	private Fornecedor fornecedor;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CompraPedido other = (CompraPedido) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}