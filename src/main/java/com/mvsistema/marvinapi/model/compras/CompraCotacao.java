package com.mvsistema.marvinapi.model.compras;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name="compra_cotacao")
@Getter
@Setter
public class CompraCotacao implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 9186770893539532475L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_compra_cotacao")
	private Long id;

	private LocalDate dataCotacao;

	private String descricao;

	private String situacao;
	
	@OneToMany(mappedBy = "compraCotacao", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<CompraReqCotacaoDetalhe> listaCompraReqCotacaoDetalhe = new ArrayList<CompraReqCotacaoDetalhe>();
	
	@OneToMany(mappedBy = "compraCotacao", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<CompraFornecedorCotacao> listaCompraFornecedorCotacao = new ArrayList<CompraFornecedorCotacao>();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CompraCotacao other = (CompraCotacao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}