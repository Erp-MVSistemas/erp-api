package com.mvsistema.marvinapi.model.compras;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mvsistema.marvinapi.model.cadastro.Produto;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="compra_requisicao_detalhe")
@Getter
@Setter
public class CompraRequisicaoDetalhe implements Serializable {
	
	private static final long serialVersionUID = 5201678925461883848L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_compra_req_det")
	private Long id;

	private String itemCotado;

	@Column(precision = 19, scale = 8)
	private BigDecimal quantidade;

	@Column(precision = 19, scale = 8)
	private BigDecimal quantidadeCotada;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="id_compra_requisicao")
	private CompraRequisicao compraRequisicao = new CompraRequisicao();

	@ManyToOne
	@JoinColumn(name="id_produto")
	private Produto produto = new Produto();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CompraRequisicaoDetalhe other = (CompraRequisicaoDetalhe) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}