package com.mvsistema.marvinapi.model.compras;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mvsistema.marvinapi.model.cadastro.Produto;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="compra_cotacao_detalhe")
@Getter
@Setter
public class CompraCotacaoDetalhe implements Serializable {
	
	private static final long serialVersionUID = -7268183300485857183L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_compra_cot_det")
	private Long id;

	@Column(precision = 19, scale = 8)
	private BigDecimal quantidade;

	@Column(name = "quantidade_pedida", precision = 19, scale = 8)
	private BigDecimal quantidadePedida;

	@Column(name = "taxa_desconto", precision = 19, scale = 8)
	private BigDecimal taxaDesconto;

	@Column(name = "valor_desconto", precision = 19, scale = 8)
	private BigDecimal valorDesconto;

	@Column(name = "valor_subtotal", precision = 19, scale = 8)
	private BigDecimal valorSubtotal;

	@Column(name = "valor_total", precision = 19, scale = 8)
	private BigDecimal valorTotal;

	@Column(name = "valor_unitario", precision = 19, scale = 8)
	private BigDecimal valorUnitario;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="id_compra_forn_cot")
	private CompraFornecedorCotacao compraFornecedorCotacao = new CompraFornecedorCotacao();

	@ManyToOne
	@JoinColumn(name="id_produto")
	private Produto produto = new Produto();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CompraCotacaoDetalhe other = (CompraCotacaoDetalhe) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}