package com.mvsistema.marvinapi.model.compras;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;


/**
 * The persistent class for the compra_req_cotacao_detalhe database table.
 * 
 */
@Entity
@Table(name="compra_req_cotacao_detalhe")
@Getter
@Setter
public class CompraReqCotacaoDetalhe implements Serializable {
	
	private static final long serialVersionUID = -999314938381318483L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_compra_req_cot_det")
	private Long id;

	@Column(precision = 19, scale = 8)
	private BigDecimal quantidadeCotada;

	@ManyToOne
	@JoinColumn(name="id_compra_req_det")
	private CompraRequisicaoDetalhe compraRequisicaoDetalhe;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="id_compra_cotacao")
	private CompraCotacao compraCotacao;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CompraReqCotacaoDetalhe other = (CompraReqCotacaoDetalhe) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}