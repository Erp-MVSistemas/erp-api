package com.mvsistema.marvinapi.model.cadastro;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name="colaborador")
@Getter
@Setter
public class Colaborador implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -1994205172916365596L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_colaborador")
	private Long id;

	private LocalDate ctpsDataExpedicao;

	private String ctpsNumero;

	private String ctpsSerie;

	private String ctpsUf;

	private LocalDate dataAdmissao;

	private LocalDate dataCadastro;

	private LocalDate dataDemissao;

	@Lob
	private String foto34;

	private String matricula;

	private String observacao;

	private String pagamentoAgencia;

	private String pagamentoAgenciaDigito;

	private String pagamentoBanco;

	private String pagamentoConta;

	private String pagamentoContaDigito;

	private String pagamentoForma;

	@ManyToOne
	@JoinColumn(name = "id_setor", nullable = true)
	private Setor setor = new Setor();

	@ManyToOne
	@JoinColumn(name = "id_cargo", nullable = true)
	private Cargo cargo = new Cargo();

	@ManyToOne
	@JoinColumn(name = "id_tipo_colaborador", nullable = true)
	private TipoColaborador tipoColaborador = new TipoColaborador();

	@ManyToOne
	@JoinColumn(name = "id_pessoa", nullable = true)
	private Pessoa pessoa = new Pessoa();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Colaborador other = (Colaborador) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}