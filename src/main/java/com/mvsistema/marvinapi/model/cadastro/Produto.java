package com.mvsistema.marvinapi.model.cadastro;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "produto")
@Getter
@Setter
public class Produto implements Serializable {
	
	private static final long serialVersionUID = 6394165724194850955L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_produto")
	private Long id;

	private Integer codigoBalanca;

	private String codigoInterno;

	private Date dataCadastro;

	private String descricao;

	private String descricaoPdv;

	private BigDecimal estoqueIdeal;

	private BigDecimal estoqueMaximo;

	private BigDecimal estoqueMinimo;

	private String excluido;

	@Lob
	private String fotoProduto;

	private String gtin;

	private String iat;

	private String inativo;

	private String ippt;

	private String ncm;

	private String nome;

	@Column(precision = 19, scale = 8)
	private BigDecimal precoLucroMaximo;

	@Column(precision = 19, scale = 8)
	private BigDecimal precoLucroMinimo;

	@Column(precision = 19, scale = 8)
	private BigDecimal precoLucroZero;

	@Column(precision = 19, scale = 8)
	private BigDecimal precoVendaMinimo;

	@Column(precision = 19, scale = 8)
	private BigDecimal quantidadeEstoque;

	@Column(precision = 19, scale = 8)
	private BigDecimal quantidadeEstoqueAnterior;

	private String tipoItemSped;

	private String totalizadorParcial;

	@Column(precision = 19, scale = 8)
	private BigDecimal valorCompra;

	@Column(precision = 19, scale = 8)
	private BigDecimal valorVenda;

	@ManyToOne
	@JoinColumn(name="id_unidade_produto")
	private UnidadeProduto unidadeProduto;

	@ManyToOne
	@JoinColumn(name="id_sub_grupo")
	private ProdutoSubGrupo produtoSubGrupo;

	@ManyToOne
	@JoinColumn(name="id_produto_marca")
	private ProdutoMarca produtoMarca;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}