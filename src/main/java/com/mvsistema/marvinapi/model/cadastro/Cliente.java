package com.mvsistema.marvinapi.model.cadastro;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "cliente")
@Getter
@Setter
public class Cliente implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 2848626397212241892L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_cliente")
	private Long id;

	private LocalDate dataCadastro;

	private LocalDate desde;

	private BigDecimal limiteCredito;

	private String observacao;

	private BigDecimal porcentoDesconto;

	@ManyToOne
	@JoinColumn(name="id_atividade_for_cli", nullable = true)
	private AtividadeForCli atividadeForCli = new AtividadeForCli();

	@ManyToOne
	@JoinColumn(name="id_pessoa")
	private Pessoa pessoa = new Pessoa();

	@ManyToOne
	@JoinColumn(name="id_situacao_for_cli")
	private SituacaoForCli situacaoForCli = new SituacaoForCli();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}