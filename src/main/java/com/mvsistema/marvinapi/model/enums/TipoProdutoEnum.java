package com.mvsistema.marvinapi.model.enums;

/**
 * 
 * @author viniciusluciene
 * MVSistema - Todos os Direitos reservados.
 * 9 de mar de 2018
 */
public enum TipoProdutoEnum {
	
	V("Venda"),
	S("Serviço"),
	M("Materia Prima"),
	C("Consumo");
	
	private String descricao;
	
	TipoProdutoEnum(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
