package com.mvsistema.marvinapi.model.enums;

import lombok.Getter;

@Getter
public enum ClassificacaoPessoa {

	CLIENTE("Cliente"),
	FORNECEDOR("fornecedor"),
	TRANSPORTADO("Transportador"),
	COLABORADOR("Colaborador");
	
	private String descricao;
	
	ClassificacaoPessoa(String descricao) {
		this.descricao = descricao;
	}
}
