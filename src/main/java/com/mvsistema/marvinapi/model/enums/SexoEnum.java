package com.mvsistema.marvinapi.model.enums;

import lombok.Getter;

@Getter
public enum SexoEnum {
	M("Mascylino"),
	F("Feminino"),
	O("outros");
	
	private String descricao;
	
	SexoEnum(String descricao) {
		this.descricao = descricao;
	}
}
