package com.mvsistema.marvinapi.model.enums;

import lombok.Getter;

@Getter
public enum TipoPessoaEnum {

	PF("Pessoa Fisica"),
	PJ("Pessoa Juridica");

	private String descricao;

	TipoPessoaEnum(String descricao) {
		this.descricao = descricao;
	}
}
