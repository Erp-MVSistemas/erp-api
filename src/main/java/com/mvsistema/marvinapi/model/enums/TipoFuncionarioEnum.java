package com.mvsistema.marvinapi.model.enums;

public enum TipoFuncionarioEnum {
	
	VENDEDOR("Vendedor"),
	SUPERVISOR("Supervisor");
	
	private String descricao;
	
	TipoFuncionarioEnum(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}

}
