package com.mvsistema.marvinapi.model.enums;

import lombok.Getter;

@Getter
public enum TipoTes {

	ENTRADA("Entrada"),
	SAIDA("Saida");
	
	private String descricao;
	
	TipoTes(String descricao) {
		this.descricao = descricao;
	}
	
}
