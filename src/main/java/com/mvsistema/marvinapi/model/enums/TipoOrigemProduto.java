package com.mvsistema.marvinapi.model.enums;

public enum TipoOrigemProduto {
	
	OP0 ("0","Nacional"),
	OP1 ("1","Estrangeira - Importação direta"), 
	OP2 ("2","Estrangeira - Adquirida no mercado interno");
	
	private String descricao;
	private String codigo;
	
	private TipoOrigemProduto(String descricao, String codigo) {
		this.descricao = descricao;
		this.codigo = codigo;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public String getCodigo() {
		return codigo;
	}

}
