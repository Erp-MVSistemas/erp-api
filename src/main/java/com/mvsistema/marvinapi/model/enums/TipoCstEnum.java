package com.mvsistema.marvinapi.model.enums;

public enum TipoCstEnum {
	
	C00	 ("00" ,"Tributada integralmente"),
	C10	 ("10" ,"Tributada e com cobrança do ICMS por substituição tributária"),
	C20	 ("20" ,"Com redução de base de cálculo"),
	C30	 ("30" ,"Isenta ou não tributada e com cobrança do ICMS por substituição tributária"),
	C40	 ("40" ,"Isenta"),
	C41	 ("41" ,"Não tributada"),
	C50	 ("50" ,"Suspensão"),
	C51	 ("51" ,"Diferimento"),
	C60	 ("60" ,"ICMS cobrado anteriormente por substituição tributária"),
	C70	 ("70" ,"Com redução de base de cálculo e cobrança do ICMS por substituição tributária"),
	C90	 ("90" ,"Outras"),
	C101 ("101","Tributada pelo Simples Nacional com permissão de crédito"), 
	C102 ("102","Tributada pelo Simples Nacional sem permissão de crédito"),
	C103 ("103","Isenção do ICMS no Simples Nacional para faixa de receita bruta"),
	C201 ("201","Tributada pelo Simples Nacional com permissão de crédito e com cobrança do ICMS por substituição tributária"),
	C202 ("202","Tributada pelo Simples Nacional sem permissão de crédito e com cobrança do ICMS por substituição tributária"),
	C203 ("203","Isenção do ICMS no Simples Nacional para faixa de receita bruta e com cobrança do ICMS por substituição tributária"),
	C300 ("300","Imune"),
	C400 ("400","Não tributada pelo Simples Nacional"),
	C500 ("500","ICMS cobrado anteriormente por substituição tributária (substituído) ou por antecipação"),
	C900 ("900","Outros");
	
	private String numero;
	private String descricao;

	TipoCstEnum(String numero,String descricao) {
		this.descricao = descricao;
		this.numero = numero;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public String getNumero() {
		return numero;
	}
	
}
