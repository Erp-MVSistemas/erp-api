package com.mvsistema.marvinapi.model.enums;

/**
 * 
 * @author viniciusluciene
 * MVSistema - Todos os Direitos reservados.
 * 9 de mar de 2018
 */
public enum TipoSimNaoEnum {

	S("Sim"),
	N("Não");
	
	private String descricao;
	
	public String getDescricao() {
		return descricao;
	}
	
	TipoSimNaoEnum(String descricao) {
		this.descricao = descricao;
	}
}
