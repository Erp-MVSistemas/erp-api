package com.mvsistema.marvinapi.model.enums;

/**
 * 
 * @author viniciusluciene
 * MVSistema - Todos os Direitos reservados.
 * 9 de mar de 2018
 */
public enum TipoPlanoContas {
	R("Receita"),
	D("Despesa");
	
	private String descricao;
	
	TipoPlanoContas(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
