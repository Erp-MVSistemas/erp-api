package com.mvsistema.marvinapi.model.enums;

/**
 * 
 * @author viniciusluciene
 * MVSistema - Todos os Direitos reservados.
 * 9 de mar de 2018
 */
public enum TipoMesEnum {
	JANEIRO(1), FEVEREIRO(2), MARCO(3), ABRIL(4), MAIO(5), JUNHO(6), JULHO(7), AGOSTO(8), SETEMBRO(9), OUTUBRO(
			10), NOVEMBRO(11), DEZEMBRO(12);

	private int descricao;

	TipoMesEnum(int descricao) {
		this.descricao = descricao;
	}

	public int getDescricao() {
		return descricao;
	}
}
