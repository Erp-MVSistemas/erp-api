package com.mvsistema.marvinapi.model.enums;

import lombok.Getter;

/**
 * 
 * @author viniciusluciene
 * MVSistema - Todos os Direitos reservados.
 * 9 de mar de 2018
 */
@Getter
public enum TipoMeiosPagamentoEnum {
	
	DINHEIRO("01","Dinheiro"),
	CHEQUE("02","Cheque"),
	CARTAOCREDITO("03","Cartão de Crédito"),
	CARTAODEBITO("04","Cartão de Débito"),
	CREDITOLOJA("05","Crédito Loja"),
	VALEALIMENTACAO("10","Vale Alimentaçao"),
	VALEREFEICAO("11","Vale Refeição"),
	VALEPRESENTE("12","Vale Presente"),
	VALECOMBUSTIVEL("13","Vale Combustível"),
	DUPLICATAMERCANTIL("14","Duplicata Mercantil"),
	BOLETOBANCARIO("15","Boleto Bancário"),
	SEMPAGAMENTO("90","Sem pagamento"),
	DEPOSITOBANCARIO("99","Depósito bancário"),
	PROMISSORIA("99","Promissoria");
	
	private String codigo;
	private String descricao;
	
	TipoMeiosPagamentoEnum(String codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
}
