package com.mvsistema.marvinapi.model.enums;

/**
 * 
 * @author viniciusluciene
 * MVSistema - Todos os Direitos reservados.
 * 9 de mar de 2018
 */
public enum TipoStatusMovimento {

	V("Venda"),
	D("Devolução"),
	C("Cancelado");
	
	private String descricao;
	
	public String getDescricao() {
		return descricao;
	}
	
	TipoStatusMovimento(String descricao) {
		this.descricao = descricao;
	}
}
