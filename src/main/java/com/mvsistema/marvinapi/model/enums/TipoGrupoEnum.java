package com.mvsistema.marvinapi.model.enums;

/**
 * 
 * @author viniciusluciene
 * MVSistema - Todos os Direitos reservados.
 * 9 de mar de 2018
 */
public enum TipoGrupoEnum {
	
	GRAOS("Grãos"),
	BEBIDAS("Bebidas"),
	CARNES("CARNES");
	
	private String descricao;
	
	TipoGrupoEnum(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
