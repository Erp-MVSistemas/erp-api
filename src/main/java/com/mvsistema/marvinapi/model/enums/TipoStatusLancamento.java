package com.mvsistema.marvinapi.model.enums;

/**
 * 
 * @author viniciusluciene
 * MVSistema - Todos os Direitos reservados.
 * 9 de mar de 2018
 */
public enum TipoStatusLancamento {

	R("Receber"),
	P("Pagar"),
	Q("Quitado"),
	PQ("Parcialmente Qauitado");
	
	private String descricao;
	
	public String getDescricao() {
		return descricao;
	}
	
	TipoStatusLancamento(String descricao){
		this.descricao = descricao;
	}
}
