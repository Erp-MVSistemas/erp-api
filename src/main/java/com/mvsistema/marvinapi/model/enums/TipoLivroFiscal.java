package com.mvsistema.marvinapi.model.enums;

import lombok.Getter;

@Getter
public enum TipoLivroFiscal {

	TRIBUTADO("Tributado"),
	NAOTRIBUTADO("Não Tributado"),
	ISENTO("Isento"),
	OUTROS("Outros");
	
	private String descricao;
	
	TipoLivroFiscal(String descricao) {
		this.descricao = descricao;
	}
}
