package com.mvsistema.marvinapi.model.enums;

/**
 * 
 * @author viniciusluciene
 * MVSistema - Todos os Direitos reservados.
 * 9 de mar de 2018
 */
public enum TipoSexoEnum {
	MASCULINO("Masculino"), 
	FEMININO("Feminino");
	
	private String descricao;

	TipoSexoEnum(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	

}
