package com.mvsistema.marvinapi.model.enums;

public enum TipoIpiEnum {
	
	C00 ("00","E","Entrada com Recuperação de Crédito"),
	C01 ("01","E","Entrada Tributada com Alíquota Zero"),
	C02 ("02","E","Entrada Isenta"),
	C03 ("03","E","Entrada Não Tributada"),
	C04 ("04","E","Entrada Imune"),
	C05 ("05","E","Entrada com Suspensão"),
	C49 ("49","E","Outras Entradas"),
	C50 ("50","S","Saída Tributada"),
	C51 ("51","S","Saída Tributável com Alíquota Zero"),
	C52 ("52","S","Saída Isenta"),
	C53 ("53","S","Saída Não Tributada"),
	C54 ("54","S","Saída Imune"),
	C55 ("55","S","Saída com Suspensão"),
	C99 ("99","S","Outras Saídas");
	
	private String numero;
	private String tipo;
	private String descricao;

	TipoIpiEnum(String numero,String tipo,String descricao) {
		this.descricao = descricao;
		this.numero = numero;
		this.tipo = tipo;
	}
	
	public String getNumero() {
		return numero;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public String getTipo() {
		return tipo;
	}

}
