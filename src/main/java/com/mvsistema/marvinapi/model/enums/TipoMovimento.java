package com.mvsistema.marvinapi.model.enums;

/**
 * 
 * @author viniciusluciene
 * MVSistema - Todos os Direitos reservados.
 * 9 de mar de 2018
 */
public enum TipoMovimento {

	ORCAMENTO("Orçamento"),
	PEDIDO("Pedido"),
	ENTRADA("Entrada"),
	SAIDA("Saida");
	
	private String descricao;
	
	TipoMovimento(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
