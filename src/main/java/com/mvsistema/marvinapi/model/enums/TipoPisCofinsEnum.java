package com.mvsistema.marvinapi.model.enums;

public enum TipoPisCofinsEnum {
	
	C01 ("01","S","Operação Tributável com Alíquota Básica"),
	C02 ("02","S","Operação Tributável com Alíquota por Unidade de Medida de Produto"),
	C03 ("03","S","Operação Tributável com Alíquota por Unidade de Medida de Produto"),
	C04 ("04","S","Operação Tributável Monofásica – Revenda a Alíquota Zero"),
	C05 ("05","S","Operação Tributável por Substituição Tributária"),
	C06 ("06","S","Operação Tributável a Alíquota Zero"),
	C07 ("07","S","Operação Isenta da Contribuição"),
	C08 ("08","S","Operação sem Incidência da Contribuição"),
	C09 ("09","S","Operação com Suspensão da Contribuição"),
	C49 ("49","S","Outras Operações de Saída"),
	C50 ("50","E","Operação com Direito a Crédito – Vinculada Exclusivamente a Receita Tributada no Mercado Interno"),
	C51 ("51","E","Operação com Direito a Crédito – Vinculada Exclusivamente a Receita Não-Tributada no Mercado Interno"),
	C52 ("52","E","Operação com Direito a Crédito – Vinculada Exclusivamente a Receita de Exportação"),
	C53 ("53","E","Operação com Direito a Crédito – Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno"),
	C54 ("54","E","Operação com Direito a Crédito – Vinculada a Receitas Tributadas no Mercado Interno e de Exportação"),
	C55 ("55","E","Operação com Direito a Crédito – Vinculada a Receitas Não Tributadas Mercado Interno e de Exportação"),
	C56 ("56","E","Oper. c/ Direito a Créd. Vinculada a Rec. Tributadas e Não-Tributadas Mercado Interno e de Exportação"),
	C60 ("60","E","Crédito Presumido – Oper. de Aquisição Vinculada Exclusivamente a Rec. Tributada no Mercado Interno"),
	C61 ("61","E","Créd. Presumido – Oper. de Aquisição Vinculada Exclusivamente a Rec. Não-Tributada Mercado Interno"),
	C62 ("62","E","Crédito Presumido – Operação de Aquisição Vinculada Exclusivamente a Receita de Exportação"),
	C63 ("63","E","Créd. Presumido – Oper. de Aquisição Vinculada a Rec.Tributadas e Não-Tributadas no Mercado Interno"),
	C64 ("64","E","Créd. Presumido – Oper. de Aquisição Vinculada a Rec. Tributadas no Mercado Interno e de Exportação"),
	C65 ("65","E","Créd. Presumido – Oper. de Aquisição Vinculada a Rec. Não-Tributadas Mercado Interno e Exportação"),
	C66 ("66","E","Créd. Presumido – Oper. de Aquisição Vinculada a Rec. Trib. e Não-Trib. Mercado Interno e Exportação"),
	C67 ("67","E","Crédito Presumido – Outras Operações"),
	C70 ("70","E","Operação de Aquisição sem Direito a Crédito"),
	C71 ("71","E","Operação de Aquisição com Isenção"),
	C72 ("72","E","Operação de Aquisição com Suspensão"),
	C73 ("73","E","Operação de Aquisição a Alíquota Zero"),
	C74 ("74","E","Operação de Aquisição sem Incidência da Contribuição"),
	C75 ("75","E","Operação de Aquisição por Substituição Tributária"),
	C98 ("98","E","Outras Operações de Entrada"),
	C99 ("99","E","Outras Operações");
	
	private String numero;
	private String tipo;
	private String descricao;

	TipoPisCofinsEnum(String numero,String tipo,String descricao) {
		this.descricao = descricao;
		this.numero = numero;
		this.tipo = tipo;
	}
	
	public String getNumero() {
		return numero;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public String getTipo() {
		return tipo;
	}
	
}
