package com.mvsistema.marvinapi.model.estoque;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mvsistema.marvinapi.model.cadastro.Produto;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="estoque_reajuste_detalhe")
@Getter
@Setter
public class EstoqueReajusteDetalhe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_estoque_reajuste_detalhe")
	private Long id;

	private BigDecimal valorOriginal;

	private BigDecimal valorReajuste;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="id_estoque_reajuste_cabecalho")
	private EstoqueReajusteCabecalho estoqueReajusteCabecalho;

	@ManyToOne
	@JoinColumn(name="id_produto")
	private Produto produto;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EstoqueReajusteDetalhe other = (EstoqueReajusteDetalhe) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}