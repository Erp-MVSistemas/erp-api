package com.mvsistema.marvinapi.model.estoque;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "estoque_contagem_cabecalho")
@Getter
@Setter
public class EstoqueContagemCabecalho implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_estoque_contagem_cabecalho")
	private Long id;

	private LocalDate dataContagem;

	private String estoqueAtualizado;

	@OneToMany(mappedBy = "estoqueContagemCabecalho", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<EstoqueContagemDetalhe> listaEstoqueContagemDetalhe;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EstoqueContagemCabecalho other = (EstoqueContagemCabecalho) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}