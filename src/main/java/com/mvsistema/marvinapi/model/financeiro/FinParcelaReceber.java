package com.mvsistema.marvinapi.model.financeiro;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mvsistema.marvinapi.model.cadastro.ContaCaixa;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="fin_parcela_receber")
@Getter
@Setter
public class FinParcelaReceber implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_fin_parcela_receber")
	private Long id;

	private String boletoNossoNumero;

	private LocalDate dataEmissao;

	private LocalDate dataVencimento;

	private LocalDate descontoAte;

	private String emitiuBoleto;

	private Integer numeroParcela;

	private BigDecimal valor;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="id_fin_lancamento_receber")
	private FinLancamentoReceber finLancamentoReceber;

	@ManyToOne
	@JoinColumn(name="id_fin_status_parcela")
	private FinStatusParcela finStatusParcela;

	@ManyToOne
	@JoinColumn(name="id_conta_caixa")
	private ContaCaixa contaCaixa;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinParcelaReceber other = (FinParcelaReceber) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}