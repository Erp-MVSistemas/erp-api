package com.mvsistema.marvinapi.model.financeiro;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.mvsistema.marvinapi.model.cadastro.Cliente;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="fin_lancamento_receber")
@Getter
@Setter
public class FinLancamentoReceber implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_fin_lancamento_receber")
	private Long id;

	private LocalDate dataLancamento;

	private Integer intervaloEntreParcelas;

	private String numeroDocumento;

	private LocalDate primeiroVencimento;

	private Integer quantidadeParcela;

	private BigDecimal taxaComissao;

	private BigDecimal valorAReceber;

	private BigDecimal valorComissao;

	private BigDecimal valorTotal;

	@ManyToOne
	@JoinColumn(name="id_fin_documento_origem")
	private FinDocumentoOrigem finDocumentoOrigem;

	@ManyToOne
	@JoinColumn(name="id_cliente")
	private Cliente cliente;

	@ManyToOne
	@JoinColumn(name="id_fin_natureza_financeira")
	private FinNaturezaFinanceira finNaturezaFinanceira;

	@OneToMany(mappedBy="finLancamentoReceber", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<FinParcelaReceber> listaFinParcelaReceber;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinLancamentoReceber other = (FinLancamentoReceber) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}