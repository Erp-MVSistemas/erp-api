package com.mvsistema.marvinapi.model.financeiro;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.mvsistema.marvinapi.model.cadastro.ContaCaixa;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="fin_configuracao_boleto")
@Getter
@Setter
public class FinConfiguracaoBoleto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_fin_configuracao_boleto")
	private Long id;

	private String aceite;

	private String caminhoArquivoLogotipo;

	private String caminhoArquivoPdf;

	private String caminhoArquivoRemessa;

	private String caminhoArquivoRetorno;

	private String carteira;

	private String codigoCedente;

	private String codigoConvenio;

	private String especie;

	private String instrucao01;

	private String instrucao02;

	private String layoutRemessa;

	private String localPagamento;

	private String mensagem;

	private BigDecimal taxaMulta;

	@ManyToOne
	@JoinColumn(name="id_conta_caixa")
	private ContaCaixa contaCaixa;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinConfiguracaoBoleto other = (FinConfiguracaoBoleto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}