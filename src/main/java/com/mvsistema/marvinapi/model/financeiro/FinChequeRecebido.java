package com.mvsistema.marvinapi.model.financeiro;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.mvsistema.marvinapi.model.cadastro.Pessoa;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="fin_cheque_recebido")
@Getter
@Setter
public class FinChequeRecebido implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_fin_cheque_recebido")
	private Long id;

	private LocalDate bomPara;

	private String codigoAgencia;

	private String codigoBanco;

	private String conta;

	private String cpfCnpj;

	private LocalDate dataCompensacao;

	private LocalDate dataEmissao;

	private String nome;

	private Integer numero;

	private BigDecimal valor;

	@ManyToOne
	@JoinColumn(name="id_pessoa")
	private Pessoa pessoa;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinChequeRecebido other = (FinChequeRecebido) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}