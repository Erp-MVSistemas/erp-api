package com.mvsistema.marvinapi.model.financeiro;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.mvsistema.marvinapi.model.cadastro.ContaCaixa;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="fin_extrato_conta_banco")
@Getter
@Setter
public class FinExtratoContaBanco implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_fin_extrato_conta_banco")
	private Long id;

	private String ano;

	private String conciliado;

	private LocalDate dataBalancete;

	private LocalDate dataMovimento;

	private String documento;

	private String historico;

	private String mes;

	private String mesAno;

	private String observacao;

	private BigDecimal valor;

	@ManyToOne
	@JoinColumn(name="id_conta_caixa")
	private ContaCaixa contaCaixa;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinExtratoContaBanco other = (FinExtratoContaBanco) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}