package com.mvsistema.marvinapi.model.financeiro;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.mvsistema.marvinapi.model.cadastro.ContaCaixa;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name="fin_parcela_pagamento")
@Getter
@Setter
public class FinParcelaPagamento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_fin_parcela_pagamento")
	private Long id;

	private LocalDate dataPagamento;

	private String historico;

	private BigDecimal taxaDesconto;

	private BigDecimal taxaJuro;

	private BigDecimal taxaMulta;

	private BigDecimal valorDesconto;

	private BigDecimal valorJuro;

	private BigDecimal valorMulta;

	private BigDecimal valorPago;

	@JsonProperty(access = Access.WRITE_ONLY)
	@ManyToOne
	@JoinColumn(name="id_fin_parcela_pagar")
	private FinParcelaPagar finParcelaPagar;

	@ManyToOne
	@JoinColumn(name="id_fin_cheque_emitido")
	private FinChequeEmitido finChequeEmitido;

	@ManyToOne
	@JoinColumn(name="id_fin_tipo_pagamento")
	private FinTipoPagamento finTipoPagamento;

	@ManyToOne
	@JoinColumn(name="id_conta_caixa")
	private ContaCaixa contaCaixa;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinParcelaPagamento other = (FinParcelaPagamento) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}