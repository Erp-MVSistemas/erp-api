package com.mvsistema.marvinapi.model.vendas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.mvsistema.marvinapi.model.cadastro.Cliente;
import com.mvsistema.marvinapi.model.cadastro.Transportadora;
import com.mvsistema.marvinapi.model.cadastro.Vendedor;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="venda_orcamento_cabecalho")
@Getter
@Setter
public class VendaOrcamentoCabecalho implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_venda_orcamento_cabecalho")
	private Long id;

	private String codigo;

	private LocalDate dataCadastro;

	private String observacao;

	private BigDecimal taxaComissao;

	private BigDecimal taxaDesconto;

	private String tipo;

	private String tipoFrete;

	private LocalDate validade;

	private BigDecimal valorComissao;

	private BigDecimal valorDesconto;

	private BigDecimal valorFrete;

	private BigDecimal valorSubtotal;

	private BigDecimal valorTotal;

	@ManyToOne
	@JoinColumn(name="id_cliente")
	private Cliente cliente = new Cliente();

	@ManyToOne
	@JoinColumn(name="id_transportadora")
	private Transportadora transportadora;

	@ManyToOne
	@JoinColumn(name="id_venda_condicoes_pagamento")
	private VendaCondicoesPagamento vendaCondicoesPagamento = new VendaCondicoesPagamento();

	@ManyToOne
	@JoinColumn(name="id_vendedor")
	private Vendedor vendedor = new Vendedor();

	@OneToMany(mappedBy = "vendaOrcamentoCabecalho", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<VendaOrcamentoDetalhe> listaVendaOrcamentoDetalhe = new ArrayList<VendaOrcamentoDetalhe>();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VendaOrcamentoCabecalho other = (VendaOrcamentoCabecalho) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}