package com.mvsistema.marvinapi.model.vendas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.mvsistema.marvinapi.model.cadastro.Cliente;
import com.mvsistema.marvinapi.model.cadastro.Transportadora;
import com.mvsistema.marvinapi.model.cadastro.Vendedor;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name="venda_cabecalho")
@Getter
@Setter
public class VendaPedidoCabecalho implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_venda_cabecalho")
	private Long id;

	private LocalDate dataSaida;

	private LocalDate dataVenda;

	private String formaPagamento;

	private LocalDateTime horaSaida;

	private String localCobranca;

	private String localEntrega;

	private Integer numeroFatura;

	private String observacao;

	private String situacao;

	private BigDecimal taxaComissao;

	private BigDecimal taxaDesconto;

	private String tipoFrete;

	private BigDecimal valorComissao;

	private BigDecimal valorDesconto;

	private BigDecimal valorFrete;

	private BigDecimal valorSeguro;

	private BigDecimal valorSubtotal;

	private BigDecimal valorTotal;

	@ManyToOne
	@JoinColumn(name="id_cliente")
	private Cliente cliente = new Cliente();

	@ManyToOne
	@JoinColumn(name="id_tipo_nota_fiscal")
	private TipoNotaFiscal tipoNotaFiscal = new TipoNotaFiscal();

	@ManyToOne
	@JoinColumn(name="id_transportador")
	private Transportadora transportadora;

	@ManyToOne
	@JoinColumn(name="id_venda_condicoes_pagamento")
	private VendaCondicoesPagamento vendaCondicoesPagamento = new VendaCondicoesPagamento();

	@ManyToOne
	@JoinColumn(name="id_venda_orcamento_cabecalho")
	private VendaOrcamentoCabecalho vendaOrcamentoCabecalho;

	@ManyToOne
	@JoinColumn(name="id_vendedor")
	private Vendedor vendedor = new Vendedor();
	
	@OneToMany(mappedBy = "vendaCabecalho", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<VendaDetalhe> listaVendaDetalhe = new ArrayList<VendaDetalhe>();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VendaPedidoCabecalho other = (VendaPedidoCabecalho) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}