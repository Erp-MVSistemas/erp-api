package com.mvsistema.marvinapi.model.vendas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.mvsistema.marvinapi.model.cadastro.Vendedor;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="venda_comissao")
@Getter
@Setter
public class VendaComissao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_venda_comissao")
	private Long id;

	private LocalDate dataLancamento;

	private String situacao;

	private String tipoContabil;

	private BigDecimal valorComissao;

	private BigDecimal valorVenda;

	@ManyToOne
	@JoinColumn(name="id_venda_cabecalho")
	private VendaPedidoCabecalho vendaCabecalho = new VendaPedidoCabecalho();

	@ManyToOne
	@JoinColumn(name="id_vendedor")
	private Vendedor vendedor = new Vendedor();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VendaComissao other = (VendaComissao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}